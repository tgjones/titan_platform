Titan Indie Platform
====================

The Titan Platform is an initiative to create an end-to-end platform for prototyping, developing and releasing indie 
games using the Python programming language.  Games that are created for Titan are inherantly portable as they are 
written in Python and run within a host engine developed in Cython.  By using Cython for the inner workings of the 
engine we are able to provide the benefit of a simple Python API and the rapid development of an optimized C back-end -
behind the scenes the Titan engine depends on the widely used and respected SDL2 cross-platform library.

License
-------

GPLv3

https://opensource.org/licenses/GPL-3.0