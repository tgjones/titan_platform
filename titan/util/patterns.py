class Singleton(type):

    # Singleton instances
    _instances = {}

    def __call__(cls, *args, **kwargs):

        # First call to singleton, initialise instance
        if cls not in cls._instances:
            cls._instances[cls] = \
                super(Singleton, cls).__call__(*args, **kwargs)

        # Get singleton instance
        inst = cls._instances[cls]

        # If there is a __call__ wrap that
        if getattr(inst, '__call__', None) is not None:
            return inst.__call__(*args, **kwargs)

        # Otherwise just return instance
        return inst
