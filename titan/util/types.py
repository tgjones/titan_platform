class Point:

    def __init__(self, x, y):
        self._x = x
        self._y = y

    def __repr__(self):
        return '<{}.{} object: x={}, y={}' \
            .format(__class__.__module__, __class__.__name__, self._x, self._y)

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        self._y = value


class Rect:

    def __init__(self, x, y, w, h):
        self._x = x
        self._y = y
        self._w = w
        self._h = h

    def __repr__(self):
        return '<{}.{} object: x={}, y={}, w={}, h={}>' \
            .format(__class__.__module__, __class__.__name__, self._x, self._y,
                    self._w, self._h)

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        self._y = value

    @property
    def width(self):
        return self._w

    @width.setter
    def width(self, value):
        self._width = value

    @property
    def height(self):
        return self._h

    @height.setter
    def height(self, value):
        self._height = value
