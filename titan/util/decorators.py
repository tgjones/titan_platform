def on_down(func):
    """Decorator to prevent 'up'/'down' hooks triggering callback on 'up'."""

    def wrapper(*args, **kwargs):
        if 'meta' in kwargs:
            if kwargs['meta'] != 'down':
                return
        func(*args, **kwargs)

    return wrapper


def no_repeat(func):
    """Decorator to prevent repeat key presses triggering callback."""

    def wrapper(*args, **kwargs):
        if 'repeat' in kwargs:
            if kwargs['repeat']:
                return
        func(*args, **kwargs)

    return wrapper
