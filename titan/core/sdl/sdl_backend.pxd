cdef class SDLBackend:

    cdef object game
    cdef public bint running

    cdef readonly object display
    cdef readonly object audio
    cdef readonly object input

    cpdef int get_time(self)
    cpdef delay(self, int ticks)

