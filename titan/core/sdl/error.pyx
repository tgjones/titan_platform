from sdl2.SDL2 cimport *

class SDLError(AssertionError):

    def __init__(self, value=None):

        self._message = sdl_error_str()

        if value is not None:
            self._message += ': '
            self._message += repr(value)

    def __str__(self):
        return self._message

    @property
    def message(self):
        return self._message

cdef sdl_error_str():
    return str(SDL_GetError(), encoding='ascii')

cdef sdl_assert(condition, value=None):
    if not condition:
        raise SDLError(value)

cdef sdl_check(expression, value=None):
    if not expression == 0:
        raise SDLError(value)
