from cpython.pycapsule cimport *

from sdl2.SDL2 cimport *
from sdl2.SDL2_mixer cimport *
from titan.core.sdl.sdl_backend cimport SDLBackend
from titan.core.sdl.error cimport sdl_assert, sdl_check

import logging as log
from titan.core.sdl.error import SDLError

cdef class AudioSubsystem:

    def __init__(self, SDLBackend backend, int frequency=MIX_DEFAULT_FREQUENCY,
                 int max_channels=MIX_DEFAULT_CHANNELS, **kwargs):

        # Initialize SDL_mixer
        flags = 0
        if 'formats' in kwargs:
            if 'ogg' in kwargs['formats']:
                flags |= MIX_INIT_OGG
            if 'mp3' in kwargs['formats']:
                flags |= MIX_INIT_MP3
            if 'mod' in kwargs['formats']:
                flags |= MIX_INIT_MOD
            if 'flac' in kwargs['formats']:
                flags |= MIX_INIT_FLAC
        else:
            flags = MIX_INIT_OGG
        sdl_assert(Mix_Init(flags) == flags)

        self.backend = backend
        self.asset_cache = {}
        self.sound_map = {}
        self.music = NULL

        # Open audio device
        chunk_size = kwargs['chunk_size'] if 'chunk_size' in kwargs else 1024
        sdl_check(Mix_OpenAudio(frequency, MIX_DEFAULT_FORMAT, 2, chunk_size))

        # Set number of channels
        sdl_assert(Mix_AllocateChannels(max_channels) == max_channels)

    def __dealloc__(self):

        # Close audio device
        Mix_CloseAudio()

    property channels:
        def __get__(self):
            return Mix_AllocateChannels(-1)

        def __set__(self, value):
            sdl_assert(Mix_AllocateChannels(value) == value)

    def play_sound(self, sound_file, repeat=False, loops=0):

        # Load sound asset
        if sound_file not in self.asset_cache:
            path = bytes(sound_file, 'utf-8')
            chunk = Mix_LoadWAV(path)
            sdl_assert(chunk != NULL)
            self.asset_cache[sound_file] = PyCapsule_New(chunk, NULL, NULL)
        else:
            chunk = <Mix_Chunk*>PyCapsule_GetPointer(self.asset_cache[sound_file], NULL)

        # Play sound asset
        try:
            channel = Mix_PlayChannel(-1, chunk, -1 if repeat else loops)
            sdl_assert(channel != -1)
            self.sound_map[channel] = sound_file

        except SDLError as e:
            if str(e) != 'No free channels available':
                raise
            log.debug(str("Sound '{}' not played, all audio channels in use".format(sound_file)))

    def play_music(self, music_file, repeat=True, loops=0, fade=0):

        # Stop music if needed
        if self.music != NULL:
            self.stop_music()

        # Load music asset
        path = bytes(music_file, 'utf-8')
        self.music = Mix_LoadMUS(path)
        sdl_assert(self.music != NULL)

        # Play music asset
        sdl_check(Mix_FadeInMusic(self.music, -1 if repeat else loops, fade))

    def stop_music(self, fade=0):

        if self.music == NULL:
            return
        sdl_assert(Mix_FadeOutMusic(fade) == 1)
        Mix_FreeMusic(self.music)
        self.music = NULL

    def pause_music(self):

        if self.music == NULL:
            return
        if Mix_PlayingMusic():
            Mix_PauseMusic()

    def resume_music(self):

        if self.music == NULL:
            return
        Mix_ResumeMusic()

    def free_finished(self, force=False):

        # Force unload of all assets
        if force:
            finished = self.sound_map.values()
            self.sound_map = {}
            if self.music != NULL:
                self.stop_music()

        # Get finished channels and remove from channel map
        else:
            finished = [sound for channel, sound in self.sound_map.items() \
                        if not Mix_Playing(channel)]
            self.sound_map = {channel: sound for channel, sound in self.sound_map.items() \
                              if sound not in finished}

        # Free sound assets no longer in use
        for sound in finished:
            if sound not in self.sound_map.values():
                chunk = <Mix_Chunk*>PyCapsule_GetPointer(self.asset_cache[sound], NULL)
                Mix_FreeChunk(chunk)
                del self.asset_cache[sound]

        # Free music asset
        if self.music != NULL and not Mix_PlayingMusic():
            self.stop_music()
