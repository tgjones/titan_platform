from sdl2.SDL2 cimport SDL_Texture, SDL_Renderer, SDL_Window
from titan.core.sdl.subsystems.util.font cimport Font
from titan.core.sdl.subsystems.util.surface cimport Surface
from titan.core.sdl.sdl_backend cimport SDLBackend

cdef class DisplaySubsystem:
    cdef readonly object sys_messages
    cdef readonly object viewport

    cdef SDLBackend backend

    cdef object textures
    cdef object entities
    cdef str driver
    cdef bint fullscreen
    cdef tuple back_color
    cdef bint show_fps
    cdef int fps_start_ticks
    cdef int fps_frame_count
    cdef int fps_prev_frame_count
    cdef int sys_msg_timeout
    cdef Font sys_font

    cdef SDL_Renderer* renderer
    cdef SDL_Window* window
