from sdl2.SDL2_mixer cimport Mix_Music
from titan.core.sdl.sdl_backend cimport SDLBackend

cdef class AudioSubsystem:

    cdef dict asset_cache
    cdef SDLBackend backend
    cdef dict sound_map
    cdef Mix_Music* music
