from cpython.pycapsule cimport *

from sdl2.SDL2 cimport *
from titan.core.sdl.sdl_backend cimport SDLBackend
from titan.core.sdl.subsystems.util.font cimport Font
from titan.core.sdl.subsystems.util.surface cimport Surface
from titan.core.sdl.error cimport sdl_assert, sdl_check

import os
import logging as log
from collections import OrderedDict
from webcolors import name_to_rgb, hex_to_rgb

from titan.util.types import Rect
from titan.core import EventDispatcher

cdef class DisplaySubsystem:

    def __init__(self, SDLBackend backend, int width=0, int height=0,
                 bint fullscreen=True, **kwargs):

        sdl_assert(SDL_WasInit(SDL_INIT_VIDEO))

        self.backend = backend
        self.fullscreen = fullscreen
        self.sys_messages = []

        self.textures = OrderedDict()
        self.entities = OrderedDict()

        # Determine display to use (default: first display)
        ndisplays = SDL_GetNumVideoDisplays()
        sdl_assert(ndisplays > 0)
        display_idx = 0
        if 'index' in kwargs:
            if kwargs['index'] not in range(0, ndisplays):
                raise IndexError('Display index ' + kwargs['index'] + ' is not valid')
            display_idx = kwargs['index']

        # Set dimensions to viewport size by default
        cdef SDL_DisplayMode dm
        sdl_check(SDL_GetDesktopDisplayMode(display_idx, &dm))
        self.viewport = Rect(0, 0,
                             dm.w if width == 0 else width,
                             dm.h if height == 0 else height)

        # Set window options
        window_pos = SDL_WINDOWPOS_UNDEFINED
        window_opts = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL
        if self.fullscreen:
            window_pos = 0
            window_opts |= SDL_WINDOW_FULLSCREEN_DESKTOP
        if 'resizable' in kwargs and kwargs['resizable']:
            window_opts |= SDL_WINDOW_RESIZABLE

        # Create the window
        title = bytes(self.backend.game.title, 'utf-8')
        self.window = SDL_CreateWindow(title, window_pos, window_pos,
                                       width, height, window_opts)
        sdl_assert(self.window != NULL)

        # Enumerate available renderers
        ndrivers = SDL_GetNumRenderDrivers()
        sdl_assert(ndrivers > 0)
        cdef SDL_RendererInfo ri
        drivers = []
        for i in range(ndrivers):
            sdl_check(SDL_GetRenderDriverInfo(i, &ri))
            drivers.append(str(ri.name, 'utf-8'))
        log.debug('Available render drivers    : ' + str(drivers))

        # Determine driver to use (default: auto-detect)
        driver_idx = -1
        if 'driver' in kwargs:
            if kwargs['driver'] not in drivers:
                raise IndexError("Render driver '" + kwargs['driver'] + "' not valid")
            driver_idx = drivers.index(kwargs['driver'])
            log.debug('Creating context using      : ' + kwargs['driver'])

        # Set renderer options
        renderer_opts = SDL_RENDERER_ACCELERATED
        if 'vsync' in kwargs and kwargs['vsync']:
            renderer_opts |= SDL_RENDERER_PRESENTVSYNC

        # Create the renderer
        self.renderer = SDL_CreateRenderer(self.window, driver_idx, renderer_opts)
        sdl_assert(self.renderer != NULL)

        # Get information on selected render driver
        sdl_check(SDL_GetRendererInfo(self.renderer, &ri))
        self.driver = str(ri.name, 'utf-8')

        # Log render driver information
        log.debug('Render driver in use        : ' + self.driver)
        log.debug(' SDL_RENDERER_SOFTWARE      : ' + str(ri.flags & SDL_RENDERER_SOFTWARE > 0))
        log.debug(' SDL_RENDERER_ACCELERATED   : ' + str(ri.flags & SDL_RENDERER_ACCELERATED > 0))
        log.debug(' SDL_RENDERER_PRESENTVSYNC  : ' + str(ri.flags & SDL_RENDERER_PRESENTVSYNC > 0))
        log.debug(' SDL_RENDERER_TARGETTEXTURE : ' + str(ri.flags & SDL_RENDERER_TARGETTEXTURE > 0))

        # Set logical scaling on fullscreen
        SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, 'best')
        sdl_check(SDL_RenderSetLogicalSize(self.renderer, width, height))

        # Default system message timeout is 2 seconds
        self.sys_msg_timeout = kwargs['sys_msg_timeout'] if 'sys_msg_timeout' in kwargs else 2000

        # Default system font is Consolas
        sys_font_path = os.path.dirname(__file__) + os.sep + 'consolas.ttf'
        if 'sys_font' in kwargs:
            sys_font_path = kwargs['sys_font']

        # System font default size is 12
        sys_font_size = 12
        if 'sys_font_color' in kwargs:
            sys_font_color = hex_to_rgb(kwargs['sys_font_color'])

        # System font default colour is white
        sys_font_color = (255, 255, 255)
        if 'sys_font_color' in kwargs:
            sys_font_size = kwargs['sys_font_size']

        # Open system font
        self.sys_font = Font(sys_font_path, sys_font_size, sys_font_color)

        # FPS display settings
        self.show_fps = kwargs['show_fps'] if 'show_fps' in kwargs else False
        self.fps_start_ticks = 0
        self.fps_frame_count = 0
        self.fps_prev_frame_count = 0

        # Save background colour
        self.back_color = name_to_rgb('black')
        if 'back_color' in kwargs:
            self.back_color = hex_to_rgb(kwargs['back_color'])

        # Set window icon if specified
        cdef Surface surf
        if 'icon' in kwargs:
            surf = Surface(image=kwargs['icon'])
            SDL_SetWindowIcon(self.window, surf.sdl_surf)

    def __dealloc__(self):

        if self.window is not NULL:
            SDL_DestroyWindow(self.window)
            self.window = NULL

        if self.renderer is not NULL:
            SDL_DestroyRenderer(self.renderer)
            self.renderer = NULL

    # Factory

    @staticmethod
    def create_surface(w, h, color_key=None):
        return Surface(width=w, height=h, color_key=color_key)

    @staticmethod
    def create_surface_from_image(path, color_key=None):
        return Surface(image=path, color_key=color_key)

    # Viewport

    def place_viewport(self, x, y):

        self.viewport.x = x
        self.viewport.y = y

    def move_viewport(self, x, y):

        self.viewport.x += x
        self.viewport.y += y

    def resize_viewport(self, width, height):

        self.viewport.width = width
        self.viewport.height = height

    # Texture

    def load_texture(self, Surface surf, id_):

        if id_ in self.textures:
            self.unload_texture(id_)

        texture = SDL_CreateTextureFromSurface(self.renderer, surf.sdl_surf)
        sdl_assert(texture != NULL)
        self.textures[id_] = PyCapsule_New(texture, NULL, NULL)

    def unload_texture(self, id_):

        if id_ in self.textures:
            SDL_DestroyTexture(<SDL_Texture*>PyCapsule_GetPointer(self.textures[id_], NULL))
            del self.textures[id_]

    def texture_loaded(self, id_):

        return id_ in self.textures

    # Entity

    def create_entity(self, id_, x=0, y=0, z=0, w=0, h=0, u=0, v=0,
        texture_id=None):

        if id_ in self.entities:
            self.destroy_entity(id_)

        if texture_id is None:
            texture_id = id_

        # Dereference texture capsule
        capsule = self.textures[texture_id]
        texture = <SDL_Texture*>PyCapsule_GetPointer(capsule, NULL)

        # Get width
        cdef int w_ = w
        if w_ == 0:
            sdl_check(SDL_QueryTexture(texture, NULL, NULL, &w_, NULL))

        # Get height
        cdef int h_ = h
        if h_ == 0:
            sdl_check(SDL_QueryTexture(texture, NULL, NULL, NULL, &h_))

        self.entities[id_] = [x, y, z, w_, h_, u, v, capsule]

    def destroy_entity(self, id_):

        if id_ in self.entities:
            del self.entities[id_]

    def entity_exists(self, id_):
        return id_ in self.entities

    def place_entity(self, id_, x, y):

        self.entities[id_][0] = x
        self.entities[id_][1] = y

    def move_entity(self, id_, x, y):

        self.entities[id_][0] += x
        self.entities[id_][1] += y

    def entity_position(self, id_):
        return (self.entities[id_][0], self.entities[id_][1])

    def resize_entity(self, id_, w, h):

        self.entities[id_][3] = w
        self.entities[id_][4] = h

    def grow_entity(self, id_, w, h):

        self.entities[id_][3] = w
        self.entities[id_][4] = h

    def entity_size(self, id_):
        return (self.entities[id_][3], self.entities[id_][4])

    def frame_entity(self, id_, u, v):

        self.entities[id_][5] = u
        self.entities[id_][6] = v

    # Render

    def render(self):

        # Trigger render_begin event callbacks
        EventDispatcher().trigger_event('render_begin')

        # Clear back-buffer
        sdl_check(SDL_SetRenderDrawColor(self.renderer, self.back_color[0],
                                         self.back_color[1], self.back_color[2], 255))
        sdl_check(SDL_RenderClear(self.renderer))

        # Loop variables
        cdef int w, h
        cdef SDL_Rect src_rect, dst_rect
        cdef Surface surface

        # Render entities in Z order (let SDL do the clipping)
        for (x, y, z, w, h, u, v, capsule) in \
            sorted(self.entities.values(), key=lambda x: x[2], reverse=True):

            # Render entire entity
            src_rect.x = u
            src_rect.y = v
            src_rect.w = w
            src_rect.h = h

            # Adjust position based on viewport
            dst_rect.x = x - self.viewport.x
            dst_rect.y = y - self.viewport.y
            dst_rect.w = w
            dst_rect.h = h

            # Render to back-buffer
            texture = <SDL_Texture*>PyCapsule_GetPointer(capsule, NULL)
            sdl_check(SDL_RenderCopy(self.renderer, texture, &src_rect, &dst_rect))

        # Calculate and render FPS
        if self.show_fps:

            if SDL_GetTicks() - self.fps_start_ticks > 1000:

                # Reset timer and frame count
                self.fps_start_ticks = SDL_GetTicks()
                self.fps_prev_frame_count = self.fps_frame_count
                self.fps_frame_count = 0

            # Create texture for message
            msg = 'FPS: ' + str(self.fps_prev_frame_count)
            surface = self.sys_font.print_to_surface(msg)
            texture = SDL_CreateTextureFromSurface(self.renderer, surface.sdl_surf)
            sdl_assert(texture != NULL)

            # Get texture dimensions and construct destination rectangle
            sdl_check(SDL_QueryTexture(texture, NULL, NULL, &w, &h))
            dst_rect.x = self.viewport.width - w - 2
            dst_rect.y = 2
            dst_rect.w = w
            dst_rect.h = h

            # Copy to back-buffer
            sdl_check(SDL_RenderCopy(self.renderer, texture, NULL, &dst_rect))

            # Clean up texture
            SDL_DestroyTexture(texture)

            # Increment frame counter
            self.fps_frame_count += 1

        # Cull timed out system messages
        self.sys_messages = [msg for msg in self.sys_messages if msg[0] > \
                             SDL_GetTicks() - self.sys_msg_timeout]

        # Render system messages
        y = self.viewport.height
        for msg in reversed(self.sys_messages):

            # Create texture for message
            surface = self.sys_font.print_to_surface(msg[1])
            texture = SDL_CreateTextureFromSurface(self.renderer, surface.sdl_surf)
            sdl_assert(texture != NULL)

            # Get texture dimensions and construct destination rectangle
            sdl_check(SDL_QueryTexture(texture, NULL, NULL, &w, &h))
            dst_rect.x = 2
            dst_rect.y = y - h + 2
            dst_rect.w = w
            dst_rect.h = h
            y -= h

            # Copy to back-buffer
            sdl_check(SDL_RenderCopy(self.renderer, texture, NULL, &dst_rect))

            # Clean up texture
            SDL_DestroyTexture(texture)

        # Swap buffers
        SDL_RenderPresent(self.renderer)

        # Trigger render_end event callbacks
        EventDispatcher().trigger_event('render_end')
