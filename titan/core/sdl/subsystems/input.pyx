from sdl2.SDL2 cimport *
from titan.core.sdl.sdl_backend cimport SDLBackend
from titan.core.sdl.subsystems.util.controller cimport Controller
from titan.core.sdl.error cimport sdl_assert, sdl_check

import os
import logging as log
import urllib.request as req

from copy import copy
from titan.core import EventDispatcher

cdef class InputSubsystem:

    def __init__(self, SDLBackend backend):

        self.backend = backend
        self.controllers = {}

        # Retrieve game controller mapping from community site
        if not os.path.isfile('gamecontrollerdb.txt'):
            log.info('Downloading game controller mapping database')
            mappings = req.urlopen('https://raw.githubusercontent.com/gabomdq/SDL_GameControllerDB/master/gamecontrollerdb.txt')
            mappings_file = open('gamecontrollerdb.txt', 'w')
            mappings_file.write(str(mappings.read(), 'utf-8'))

        # Initialize game controller mapping from local file
        if os.path.isfile('gamecontrollerdb.txt'):
            SDL_GameControllerAddMappingsFromFile('gamecontrollerdb.txt')

        # Scan for game controllers already connected
        for device_idx in range(0, SDL_NumJoysticks()):
            if SDL_IsGameController(device_idx):
                self._connect_controller(device_idx)

        # Setup event filter for handling tablet events
        SDL_SetEventFilter(<SDL_EventFilter>filter_app_events, <void*>backend)

    def disconnect_controllers(self):

        for instance_id in self.controllers.copy().keys():
            self._disconnect_controller(instance_id)

    def _connect_controller(self, int device_idx):

        controller = Controller()
        instance_id = controller.open(device_idx)
        self.controllers[instance_id] = controller

        log.debug("Controller '" + controller.name + "' (ID: " + str(instance_id) + ") connected")

        return instance_id

    def _disconnect_controller(self, int instance_id):

        controller = self.controllers[instance_id]
        if controller.is_open():
            controller.close()
        del self.controllers[instance_id]

        log.debug("Controller '" + controller.name + "' (ID: " + str(instance_id) + ") disconnected" )

    def process_queue(self):

        cdef SDL_Event ev

        while SDL_PollEvent(&ev):

            if ev.type == SDL_QUIT:
                self.backend.running = False
                return

            #
            # Window events
            #

            elif ev.type == SDL_WINDOWEVENT:

                if ev.window.event == SDL_WINDOWEVENT_SHOWN:
                    EventDispatcher().trigger_event('window_shown')

                elif ev.window.event == SDL_WINDOWEVENT_HIDDEN:
                    EventDispatcher().trigger_event('window_hidden')

                elif ev.window.event == SDL_WINDOWEVENT_EXPOSED:
                    EventDispatcher().trigger_event('window_exposed')

                elif ev.window.event == SDL_WINDOWEVENT_MOVED:
                    EventDispatcher().trigger_event('window_moved', x=ev.window.data1, y=ev.window.data2)

                elif ev.window.event == SDL_WINDOWEVENT_RESIZED:
                    EventDispatcher().trigger_event('window_resized', width=ev.window.data1, height=ev.window.data2)

                elif ev.window.event == SDL_WINDOWEVENT_MINIMIZED:
                    EventDispatcher().trigger_event('window_minimized')

                elif ev.window.event == SDL_WINDOWEVENT_MAXIMIZED:
                    EventDispatcher().trigger_event('window_maximized')

                elif ev.window.event == SDL_WINDOWEVENT_RESTORED:
                    EventDispatcher().trigger_event('window_restored')

                elif ev.window.event == SDL_WINDOWEVENT_ENTER:
                    EventDispatcher().trigger_event('window_focus', meta='keyboard')

                elif ev.window.event == SDL_WINDOWEVENT_FOCUS_GAINED:
                    EventDispatcher().trigger_event('window_focus', meta='mouse')

                elif ev.window.event == SDL_WINDOWEVENT_LEAVE:
                    EventDispatcher().trigger_event('window_unfocus', meta='keyboard')

                elif ev.window.event == SDL_WINDOWEVENT_FOCUS_LOST:
                    EventDispatcher().trigger_event('window_unfocus', meta='mouse')

                elif ev.window.event == SDL_WINDOWEVENT_CLOSE:
                    EventDispatcher().trigger_event('window_close')

            #
            # Keyboard events
            #

            elif ev.type == SDL_KEYDOWN or ev.type == SDL_KEYUP:

                meta = 'down' if ev.type == SDL_KEYDOWN else 'up'

                if ev.key.keysym.sym == SDLK_RETURN:
                    EventDispatcher().trigger_event('key_enter', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_ESCAPE:
                    EventDispatcher().trigger_event('key_escape', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_BACKSPACE:
                    EventDispatcher().trigger_event('key_backspace', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_TAB:
                    EventDispatcher().trigger_event('key_tab', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_SPACE:
                    EventDispatcher().trigger_event('key_spacebar', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_EXCLAIM:
                    EventDispatcher().trigger_event('key_exclaim', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_QUOTEDBL:
                    EventDispatcher().trigger_event('key_dbl_quote', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_HASH:
                    EventDispatcher().trigger_event('key_hash', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_PERCENT:
                    EventDispatcher().trigger_event('key_percent', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_DOLLAR:
                    EventDispatcher().trigger_event('key_dollar', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AMPERSAND:
                    EventDispatcher().trigger_event('key_ampersand', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_QUOTE:
                    EventDispatcher().trigger_event('key_quote', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_LEFTPAREN:
                    EventDispatcher().trigger_event('key_l_paren', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_RIGHTPAREN:
                    EventDispatcher().trigger_event('key_r_paren', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_ASTERISK:
                    EventDispatcher().trigger_event('key_asterisk', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_PLUS:
                    EventDispatcher().trigger_event('key_plus', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_COMMA:
                    EventDispatcher().trigger_event('key_comma', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_MINUS:
                    EventDispatcher().trigger_event('key_minus', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_PERIOD:
                    EventDispatcher().trigger_event('key_period', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_SLASH:
                    EventDispatcher().trigger_event('key_fwd_slash', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_0:
                    EventDispatcher().trigger_event('key_0', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_1:
                    EventDispatcher().trigger_event('key_1', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_2:
                    EventDispatcher().trigger_event('key_2', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_3:
                    EventDispatcher().trigger_event('key_3', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_4:
                    EventDispatcher().trigger_event('key_4', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_5:
                    EventDispatcher().trigger_event('key_5', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_6:
                    EventDispatcher().trigger_event('key_6', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_7:
                    EventDispatcher().trigger_event('key_7', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_8:
                    EventDispatcher().trigger_event('key_8', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_9:
                    EventDispatcher().trigger_event('key_9', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_COLON:
                    EventDispatcher().trigger_event('key_colon', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_SEMICOLON:
                    EventDispatcher().trigger_event('key_semicolon', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_LESS:
                    EventDispatcher().trigger_event('key_less', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_EQUALS:
                    EventDispatcher().trigger_event('key_equals', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_GREATER:
                    EventDispatcher().trigger_event('key_greater', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_QUESTION:
                    EventDispatcher().trigger_event('key_question', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_LEFTBRACKET:
                    EventDispatcher().trigger_event('key_l_sq_bracket', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_BACKSLASH:
                    EventDispatcher().trigger_event('key_back_slash', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_RIGHTBRACKET:
                    EventDispatcher().trigger_event('key_r_sq_bracket', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_CARET:
                    EventDispatcher().trigger_event('key_caret', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_UNDERSCORE:
                    EventDispatcher().trigger_event('key_underscore', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_BACKQUOTE:
                    EventDispatcher().trigger_event('key_back_quote', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_a:
                    EventDispatcher().trigger_event('key_a', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_b:
                    EventDispatcher().trigger_event('key_b', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_c:
                    EventDispatcher().trigger_event('key_d', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_e:
                    EventDispatcher().trigger_event('key_e', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_f:
                    EventDispatcher().trigger_event('key_f', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_g:
                    EventDispatcher().trigger_event('key_g', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_h:
                    EventDispatcher().trigger_event('key_h', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_i:
                    EventDispatcher().trigger_event('key_i', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_j:
                    EventDispatcher().trigger_event('key_j', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_k:
                    EventDispatcher().trigger_event('key_k', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_l:
                    EventDispatcher().trigger_event('key_l', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_m:
                    EventDispatcher().trigger_event('key_m', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_n:
                    EventDispatcher().trigger_event('key_n', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_o:
                    EventDispatcher().trigger_event('key_o', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_p:
                    EventDispatcher().trigger_event('key_p', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_q:
                    EventDispatcher().trigger_event('key_q', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_r:
                    EventDispatcher().trigger_event('key_r', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_s:
                    EventDispatcher().trigger_event('key_s', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_t:
                    EventDispatcher().trigger_event('key_t', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_u:
                    EventDispatcher().trigger_event('key_u', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_v:
                    EventDispatcher().trigger_event('key_v', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_w:
                    EventDispatcher().trigger_event('key_w', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_x:
                    EventDispatcher().trigger_event('key_w', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_y:
                    EventDispatcher().trigger_event('key_w', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_z:
                    EventDispatcher().trigger_event('key_w', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_CAPSLOCK:
                    EventDispatcher().trigger_event('key_caps_lock', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F1:
                    EventDispatcher().trigger_event('key_f1', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F2:
                    EventDispatcher().trigger_event('key_f2', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F3:
                    EventDispatcher().trigger_event('key_f3', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F4:
                    EventDispatcher().trigger_event('key_f4', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F5:
                    EventDispatcher().trigger_event('key_f5', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F6:
                    EventDispatcher().trigger_event('key_f6', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F7:
                    EventDispatcher().trigger_event('key_f7', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F8:
                    EventDispatcher().trigger_event('key_f8', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F9:
                    EventDispatcher().trigger_event('key_f9', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F10:
                    EventDispatcher().trigger_event('key_f10', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F11:
                    EventDispatcher().trigger_event('key_f11', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F12:
                    EventDispatcher().trigger_event('key_f12', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_PRINTSCREEN:
                    EventDispatcher().trigger_event('key_print_screen', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_SCROLLLOCK:
                    EventDispatcher().trigger_event('key_scroll_lock', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_PAUSE:
                    EventDispatcher().trigger_event('key_pause', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_INSERT:
                    EventDispatcher().trigger_event('key_insert', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_HOME:
                    EventDispatcher().trigger_event('key_home', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_PAGEUP:
                    EventDispatcher().trigger_event('key_page_up', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_DELETE:
                    EventDispatcher().trigger_event('key_delete', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_END:
                    EventDispatcher().trigger_event('key_end', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_PAGEDOWN:
                    EventDispatcher().trigger_event('key_page_down', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_RIGHT:
                    EventDispatcher().trigger_event('key_right', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_LEFT:
                    EventDispatcher().trigger_event('key_left', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_DOWN:
                    EventDispatcher().trigger_event('key_down', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_UP:
                    EventDispatcher().trigger_event('key_up', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_NUMLOCKCLEAR:
                    EventDispatcher().trigger_event('key_num_lock', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_DIVIDE:
                    EventDispatcher().trigger_event('key_kp_divide', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_MULTIPLY:
                    EventDispatcher().trigger_event('key_kp_multiply', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_MINUS:
                    EventDispatcher().trigger_event('key_kp_minus', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_ENTER:
                    EventDispatcher().trigger_event('key_kp_enter', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_1:
                    EventDispatcher().trigger_event('key_kp_1', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_2:
                    EventDispatcher().trigger_event('key_kp_2', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_3:
                    EventDispatcher().trigger_event('key_kp_3', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_4:
                    EventDispatcher().trigger_event('key_kp_4', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_5:
                    EventDispatcher().trigger_event('key_kp_5', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_6:
                    EventDispatcher().trigger_event('key_kp_6', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_7:
                    EventDispatcher().trigger_event('key_kp_7', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_8:
                    EventDispatcher().trigger_event('key_kp_8', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_9:
                    EventDispatcher().trigger_event('key_kp_9', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_0:
                    EventDispatcher().trigger_event('key_kp_0', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_PERIOD:
                    EventDispatcher().trigger_event('key_kp_decimal', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_APPLICATION:
                    EventDispatcher().trigger_event('key_application', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_POWER:
                    EventDispatcher().trigger_event('key_power', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_EQUALS:
                    EventDispatcher().trigger_event('key_kp_equals', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F13:
                    EventDispatcher().trigger_event('key_f13', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F14:
                    EventDispatcher().trigger_event('key_f14', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F15:
                    EventDispatcher().trigger_event('key_f15', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F16:
                    EventDispatcher().trigger_event('key_f16', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F17:
                    EventDispatcher().trigger_event('key_f17', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F18:
                    EventDispatcher().trigger_event('key_f18', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F19:
                    EventDispatcher().trigger_event('key_f19', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F20:
                    EventDispatcher().trigger_event('key_f20', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F21:
                    EventDispatcher().trigger_event('key_f21', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F22:
                    EventDispatcher().trigger_event('key_f22', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F23:
                    EventDispatcher().trigger_event('key_f23', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_F24:
                    EventDispatcher().trigger_event('key_f24', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_EXECUTE:
                    EventDispatcher().trigger_event('key_execute', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_HELP:
                    EventDispatcher().trigger_event('key_help', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_MENU:
                    EventDispatcher().trigger_event('key_menu', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_SELECT:
                    EventDispatcher().trigger_event('key_select', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_STOP:
                    EventDispatcher().trigger_event('key_stop', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AGAIN:
                    EventDispatcher().trigger_event('key_again', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_UNDO:
                    EventDispatcher().trigger_event('key_undo', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_CUT:
                    EventDispatcher().trigger_event('key_cut', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_COPY:
                    EventDispatcher().trigger_event('key_copy', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_PASTE:
                    EventDispatcher().trigger_event('key_paste', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_FIND:
                    EventDispatcher().trigger_event('key_find', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_MUTE:
                    EventDispatcher().trigger_event('key_mute', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_VOLUMEUP:
                    EventDispatcher().trigger_event('key_volume_up', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_VOLUMEDOWN:
                    EventDispatcher().trigger_event('key_volume_down', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_COMMA:
                    EventDispatcher().trigger_event('key_kp_comma', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_EQUALSAS400:
                    EventDispatcher().trigger_event('key_kp_equals_as400', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_ALTERASE:
                    EventDispatcher().trigger_event('key_alt_erase', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_SYSREQ:
                    EventDispatcher().trigger_event('key_sysreq', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_CANCEL:
                    EventDispatcher().trigger_event('key_cancel', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_CLEAR:
                    EventDispatcher().trigger_event('key_clear', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_RETURN2:
                    EventDispatcher().trigger_event('key_return', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_OUT:
                    EventDispatcher().trigger_event('key_out', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_OPER:
                    EventDispatcher().trigger_event('key_oper', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_CLEARAGAIN:
                    EventDispatcher().trigger_event('key_clear_again', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_CRSEL:
                    EventDispatcher().trigger_event('key_crsel', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_EXSEL:
                    EventDispatcher().trigger_event('key_exsel', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_00:
                    EventDispatcher().trigger_event('key_kp_00', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_000:
                    EventDispatcher().trigger_event('key_kp_000', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_THOUSANDSSEPARATOR:
                    EventDispatcher().trigger_event('key_thousand_separator', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_DECIMALSEPARATOR:
                    EventDispatcher().trigger_event('key_decimal_seperator', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_CURRENCYUNIT:
                    EventDispatcher().trigger_event('key_currency_unit', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_CURRENCYSUBUNIT:
                    EventDispatcher().trigger_event('key_currency_subunit', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_LEFTPAREN:
                    EventDispatcher().trigger_event('key_kp_l_paren', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_RIGHTPAREN:
                    EventDispatcher().trigger_event('key_kp_r_paren', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_LEFTBRACE:
                    EventDispatcher().trigger_event('key_kp_l_brace', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_RIGHTBRACE:
                    EventDispatcher().trigger_event('key_kp_r_brace', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_TAB:
                    EventDispatcher().trigger_event('key_kp_tab', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_BACKSPACE:
                    EventDispatcher().trigger_event('key_kp_backspace', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_A:
                    EventDispatcher().trigger_event('key_kp_a', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_B:
                    EventDispatcher().trigger_event('key_kp_b', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_C:
                    EventDispatcher().trigger_event('key_kp_c', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_D:
                    EventDispatcher().trigger_event('key_kp_d', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_E:
                    EventDispatcher().trigger_event('key_kp_e', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_F:
                    EventDispatcher().trigger_event('key_kp_f', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_XOR:
                    EventDispatcher().trigger_event('key_kp_xor', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_POWER:
                    EventDispatcher().trigger_event('key_kp_power', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_PERCENT:
                    EventDispatcher().trigger_event('key_kp_percent', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_LESS:
                    EventDispatcher().trigger_event('key_kp_less', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_GREATER:
                    EventDispatcher().trigger_event('key_kp_greater', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_AMPERSAND:
                    EventDispatcher().trigger_event('key_kp_ampersand', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_DBLAMPERSAND:
                    EventDispatcher().trigger_event('key_kp_dbl_ampersand', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_VERTICALBAR:
                    EventDispatcher().trigger_event('key_kp_vertical_bar', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_DBLVERTICALBAR:
                    EventDispatcher().trigger_event('key_kp_dbl_vertical_bar', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_COLON:
                    EventDispatcher().trigger_event('key_kp_colon', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_HASH:
                    EventDispatcher().trigger_event('key_kp_hash', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_SPACE:
                    EventDispatcher().trigger_event('key_kp_space', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_AT:
                    EventDispatcher().trigger_event('key_kp_at', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_EXCLAM:
                    EventDispatcher().trigger_event('key_kp_exclamation', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_MEMSTORE:
                    EventDispatcher().trigger_event('key_kp_mem_store', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_MEMRECALL:
                    EventDispatcher().trigger_event('key_kp_mem_recall', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_MEMCLEAR:
                    EventDispatcher().trigger_event('key_kp_mem_clear', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_MEMADD:
                    EventDispatcher().trigger_event('key_kp_mem_add', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_MEMSUBTRACT:
                    EventDispatcher().trigger_event('key_kp_mem_subtract', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_MEMMULTIPLY:
                    EventDispatcher().trigger_event('key_kp_mem_multiply', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_MEMDIVIDE:
                    EventDispatcher().trigger_event('key_kp_mem_divide', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_PLUSMINUS:
                    EventDispatcher().trigger_event('key_kp_plus_minus', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_CLEAR:
                    EventDispatcher().trigger_event('key_kp_clear', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_CLEARENTRY:
                    EventDispatcher().trigger_event('key_kp_clear_entry', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_BINARY:
                    EventDispatcher().trigger_event('key_kp_binary', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_OCTAL:
                    EventDispatcher().trigger_event('key_kp_octal', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_DECIMAL:
                    EventDispatcher().trigger_event('key_kp_decimal', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KP_HEXADECIMAL:
                    EventDispatcher().trigger_event('key_kp_hexadecimal', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_LCTRL:
                    EventDispatcher().trigger_event('key_l_ctrl', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_LSHIFT:
                    EventDispatcher().trigger_event('key_l_shift', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_LALT:
                    EventDispatcher().trigger_event('key_l_alt', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_LGUI:
                    EventDispatcher().trigger_event('key_l_meta', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_RCTRL:
                    EventDispatcher().trigger_event('key_r_ctrl', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_RSHIFT:
                    EventDispatcher().trigger_event('key_r_shift', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_RALT:
                    EventDispatcher().trigger_event('key_r_alt', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_RGUI:
                    EventDispatcher().trigger_event('key_r_meta', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_MODE:
                    EventDispatcher().trigger_event('key_mode', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AUDIONEXT:
                    EventDispatcher().trigger_event('key_audio_next', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AUDIOPREV:
                    EventDispatcher().trigger_event('key_audio_prev', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AUDIOSTOP:
                    EventDispatcher().trigger_event('key_audio_stop', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AUDIOPLAY:
                    EventDispatcher().trigger_event('key_audio_play', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AUDIOMUTE:
                    EventDispatcher().trigger_event('key_audio_mute', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_MEDIASELECT:
                    EventDispatcher().trigger_event('key_media_select', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_WWW:
                    EventDispatcher().trigger_event('key_www', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_MAIL:
                    EventDispatcher().trigger_event('key_mail', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_CALCULATOR:
                    EventDispatcher().trigger_event('key_calculator', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AC_SEARCH:
                    EventDispatcher().trigger_event('key_ac_search', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AC_HOME:
                    EventDispatcher().trigger_event('key_ac_home', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AC_BACK:
                    EventDispatcher().trigger_event('key_ac_back', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AC_FORWARD:
                    EventDispatcher().trigger_event('key_ac_forward', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AC_STOP:
                    EventDispatcher().trigger_event('key_ac_stop', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AC_REFRESH:
                    EventDispatcher().trigger_event('key_ac_refresh', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_AC_BOOKMARKS:
                    EventDispatcher().trigger_event('key_ac_bookmarks', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_BRIGHTNESSDOWN:
                    EventDispatcher().trigger_event('key_brightness_down', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_BRIGHTNESSUP:
                    EventDispatcher().trigger_event('key_brightness_up', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_DISPLAYSWITCH:
                    EventDispatcher().trigger_event('key_display_switch', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KBDILLUMDOWN:
                    EventDispatcher().trigger_event('key_kbd_illum_down', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_KBDILLUMUP:
                    EventDispatcher().trigger_event('key_kbd_illum_up', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_EJECT:
                    EventDispatcher().trigger_event('key_eject', meta=meta, repeat=ev.key.repeat != 0)

                elif ev.key.keysym.sym == SDLK_SLEEP:
                    EventDispatcher().trigger_event('key_sleep', meta=meta, repeat=ev.key.repeat != 0)

            #
            # Mouse events
            #

            elif ev.type == SDL_MOUSEMOTION:

                if ev.motion.which != SDL_TOUCH_MOUSEID:
                    EventDispatcher().trigger_event('mouse_move', x=ev.motion.x, y=ev.motion.y, xrel=ev.motion.xrel, yrel=ev.motion.yrel)

            elif ev.type == SDL_MOUSEBUTTONDOWN or ev.type == SDL_MOUSEBUTTONUP:

                meta = 'down' if ev.type == SDL_MOUSEBUTTONDOWN else 'up'

                if ev.button.button == SDL_BUTTON_LEFT:
                    EventDispatcher().trigger_event('mouse_btn_1', meta=meta, x=ev.button.x, y=ev.button.y)

                elif ev.button.button == SDL_BUTTON_MIDDLE:
                    EventDispatcher().trigger_event('mouse_btn_2', meta=meta, x=ev.button.x, y=ev.button.y)

                elif ev.button.button == SDL_BUTTON_RIGHT:
                    EventDispatcher().trigger_event('mouse_btn_3', meta=meta, x=ev.button.x, y=ev.button.y)

                elif ev.button.button == SDL_BUTTON_X1:
                    EventDispatcher().trigger_event('mouse_btn_4', meta=meta, x=ev.button.x, y=ev.button.y)

                elif ev.button.button == SDL_BUTTON_X2:
                    EventDispatcher().trigger_event('mouse_btn_5', meta=meta, x=ev.button.x, y=ev.button.y)

            elif ev.type == SDL_MOUSEWHEEL:

                if ev.wheel.which != SDL_TOUCH_MOUSEID:
                    EventDispatcher().trigger_event('mouse_wheel', x=ev.wheel.x, y=ev.wheel.y)

            #
            # Game controller events
            #

            elif ev.type == SDL_CONTROLLERAXISMOTION:

                if ev.caxis.axis == SDL_CONTROLLER_AXIS_LEFTX:
                    EventDispatcher().trigger_event('controller_left_axis', meta='x', value=ev.caxis.value, id=ev.caxis.which)

                elif ev.caxis.axis == SDL_CONTROLLER_AXIS_LEFTY:
                    EventDispatcher().trigger_event('controller_left_axis', meta='y', value=ev.caxis.value, id=ev.caxis.which)

                elif ev.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTX:
                    EventDispatcher().trigger_event('controller_right_axis', meta='x', value=ev.caxis.value, id=ev.caxis.which)

                elif ev.caxis.axis == SDL_CONTROLLER_AXIS_RIGHTY:
                    EventDispatcher().trigger_event('controller_right_axis', meta='y', value=ev.caxis.value, id=ev.caxis.which)

                elif ev.caxis.axis == SDL_CONTROLLER_AXIS_TRIGGERLEFT:
                    EventDispatcher().trigger_event('controller_left_trigger', value=ev.caxis.value, id=ev.caxis.which)

                elif ev.caxis.axis == SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
                    EventDispatcher().trigger_event('controller_right_trigger', value=ev.caxis.value, id=ev.caxis.which)

            elif ev.type == SDL_CONTROLLERBUTTONDOWN or ev.type == SDL_CONTROLLERBUTTONUP:

                meta = 'down' if ev.type == SDL_CONTROLLERBUTTONDOWN else 'up'

                if ev.cbutton.button == SDL_CONTROLLER_BUTTON_A:
                    EventDispatcher().trigger_event('controller_btn_a', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_B:
                    EventDispatcher().trigger_event('controller_btn_b', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_X:
                    EventDispatcher().trigger_event('controller_btn_x', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_Y:
                    EventDispatcher().trigger_event('controller_btn_y', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_BACK:
                    EventDispatcher().trigger_event('controller_btn_back', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_GUIDE:
                    EventDispatcher().trigger_event('controller_btn_guide', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_START:
                    EventDispatcher().trigger_event('controller_btn_start', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_LEFTSTICK:
                    EventDispatcher().trigger_event('controller_l_stick', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_RIGHTSTICK:
                    EventDispatcher().trigger_event('controller_r_stick', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
                    EventDispatcher().trigger_event('controller_l_bumper', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
                    EventDispatcher().trigger_event('controller_r_bumper', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_UP:
                    EventDispatcher().trigger_event('controller_dpad_up', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_DOWN:
                    EventDispatcher().trigger_event('controller_dpad_down', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_LEFT:
                    EventDispatcher().trigger_event('controller_dpad_left', meta=meta, id=ev.cbutton.which)

                elif ev.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
                    EventDispatcher().trigger_event('controller_dpad_right', meta=meta, id=ev.cbutton.which)

            elif ev.type == SDL_CONTROLLERDEVICEADDED:

                instance_id = self._connect_controller(ev.cdevice.which)
                EventDispatcher().trigger_event('controller_connected', id=instance_id, joystick_idx=ev.cdevice.which)

            elif ev.type == SDL_CONTROLLERDEVICEREMOVED:

                self._disconnect_controller(ev.cdevice.which)
                EventDispatcher().trigger_event('controller_disconnected', id=ev.cdevice.which)

            #
            # Touch events
            #

            elif ev.type == SDL_FINGERDOWN:
                EventDispatcher().trigger_event('finger_down', meta=ev.tfinger.fingerId, x=ev.tfinger.x, y=ev.tfinger.y, pressure=ev.tfinger.pressure)

            elif ev.type == SDL_FINGERUP:
                EventDispatcher().trigger_event('finger_up', meta=ev.tfinger.fingerId, x=ev.tfinger.x, y=ev.tfinger.y)

            elif ev.type == SDL_FINGERMOTION:
                EventDispatcher().trigger_event('finger_move', meta=ev.tfinger.fingerId, x=ev.tfinger.x, y=ev.tfinger.y, dx=ev.tfinger.dx, dy=ev.tfinger.dy)


cdef int filter_app_events(void* userdata, SDL_Event* ev):

    backend = <SDLBackend>userdata

    if ev.type == SDL_APP_TERMINATING:
        # Terminate the app.
        # Shut everything down before returning from this function.
        return 0

    elif ev.type == SDL_APP_LOWMEMORY:
        # You will get this when your app is paused and iOS wants more memory.
        # Release as much memory as possible.
        return 0

    elif ev.type == SDL_APP_WILLENTERBACKGROUND:
        # Prepare your app to go into the background.  Stop loops, etc.
        # This gets called when the user hits the home button, or gets a call.
        return 0

    elif ev.type == SDL_APP_DIDENTERBACKGROUND:
        # This will get called if the user accepted whatever sent your app to the background.
        # If the user got a phone call and canceled it, you'll instead get an    SDL_APP_DIDENTERFOREGROUND event and restart your loops.
        # When you get this, you have 5 seconds to save all your state or the app will be terminated.
        # Your app is NOT active at this point.
        return 0

    elif ev.type == SDL_APP_WILLENTERFOREGROUND:
        #  This call happens when your app is coming back to the foreground.
        # Restore all your state here.
        return 0

    elif ev.type == SDL_APP_DIDENTERFOREGROUND:
        # Restart your loops here.
        # Your app is interactive and getting CPU again.
        return 0

    return 1
