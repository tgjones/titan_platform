from sdl2.SDL2 cimport *
from titan.core.sdl.error cimport sdl_assert

cdef class Controller:

    def __init__(self):

        sdl_assert(SDL_WasInit(SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC))
        self.controller = NULL
        # self.haptic = NULL

    cpdef is_open(self):
        return self.controller != NULL

    cpdef SDL_JoystickID open(self, int device_idx):

        if self.is_open():
            return self.instance_id

        if not SDL_IsGameController(device_idx):
            raise EnvironmentError('Joystick ' + device_idx + ' does not support Game Controller API')

        self.controller = SDL_GameControllerOpen(device_idx)
        sdl_assert(self.controller != NULL)

        # Get implementation specific controller name
        name = SDL_GameControllerName(self.controller)
        sdl_assert(name != NULL)
        self.name = str(name, 'utf-8')

        # Get joystick instance ID
        cdef SDL_Joystick* j = SDL_GameControllerGetJoystick(self.controller)
        self.instance_id = SDL_JoystickInstanceID(j)

        # Enable force feedback if supported
        # if SDL_JoystickIsHaptic(SDL_Joystick* j):
        #     self.haptic = SDL_HapticOpenFromJoystick(j)
        #     sdl_assert(self.haptic != NULL)

        return self.instance_id

    cpdef close(self):

        if not self.is_open():
            return

        # if self.haptic != NULL:
        #    SDL_HapticClose(self.haptic)
        #    self.haptic = NULL

        SDL_GameControllerClose(self.controller)
        self.controller = NULL
