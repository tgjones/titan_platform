from sdl2.SDL2 cimport *
from sdl2.SDL2_image cimport IMG_Load
from titan.core.sdl.error cimport sdl_assert, sdl_check

import os


cdef class Surface:

    def __init__(self, image=None, width=0, height=0, color_key=None):

        # Load surface from image on disk
        if isinstance(image, str):
            if not os.path.isfile(image):
                raise IOError('Image file not found: %s' % image)
            self.sdl_surf = IMG_Load(bytes(image, 'utf-8'))
            sdl_assert(self.sdl_surf != NULL)

        # Otherwise create a blank surface
        elif width > 0 and height > 0:
            self.sdl_surf = SDL_CreateRGBSurface(0, width, height, 32,
                                                 0xFF000000, 0x00FF0000,
                                                 0x0000FF00, 0x000000FF)
            sdl_assert(self.sdl_surf != NULL)

        self.width = self.sdl_surf.w
        self.height = self.sdl_surf.h

        # Enable color key if specified
        if color_key is not None:
            SDL_SetColorKey(self.sdl_surf, SDL_TRUE,
                             SDL_MapRGB(self.sdl_surf.format,
                                        color_key[0],
                                        color_key[1],
                                        color_key[2]))

    def __dealloc__(self):

        if self.sdl_surf == NULL:
            SDL_FreeSurface(self.sdl_surf)

    cpdef fill(self, r, g, b, a=0):

        SDL_FillRect(self.sdl_surf, NULL,
                     SDL_MapRGBA(self.sdl_surf.format, r, g, b, a))

    cpdef blit(self, Surface surf, x=0, y=0, src=None):

        cdef SDL_Rect _src_rect
        cdef SDL_Rect _dst_rect

        # Construct source rectangle
        if src is None:
            _src_rect.x = 0
            _src_rect.y = 0
            _src_rect.w = surf.sdl_surf.w
            _src_rect.h = surf.sdl_surf.h
        else:
            _src_rect.x = src.x
            _src_rect.y = src.y
            _src_rect.w = src.width
            _src_rect.h = src.height

        # Construct destination rectangle (only position is used by SDL)
        _dst_rect.x = x
        _dst_rect.y = y

        # Copy pixels
        sdl_check(SDL_BlitSurface(surf.sdl_surf, &_src_rect, self.sdl_surf, &_dst_rect))

