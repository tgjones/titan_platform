from sdl2.SDL2 cimport *
from sdl2.SDL2_ttf cimport *

cdef class Font:
    cdef readonly int size
    cdef readonly tuple color
    cdef SDL_Color sdl_color
    cdef TTF_Font* sdl_font

    cpdef print_to_surface(self, msg)
