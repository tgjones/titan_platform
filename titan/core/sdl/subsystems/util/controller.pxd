from sdl2.SDL2 cimport SDL_JoystickID, SDL_GameController

cdef class Controller:

    cdef readonly str name
    cdef readonly SDL_JoystickID instance_id

    cdef SDL_GameController* controller
    # cdef SDL_Haptic* haptic

    cpdef is_open(self)
    cpdef SDL_JoystickID open(self, int device_idx)
    cpdef close(self)
