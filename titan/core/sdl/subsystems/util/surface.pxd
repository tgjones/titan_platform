from sdl2.SDL2 cimport SDL_Surface

cdef class Surface:
    cdef SDL_Surface* sdl_surf
    cdef readonly int width
    cdef readonly int height

    cpdef fill(self, r, g, b, a=*)
    cpdef blit(self, Surface surf, x=*, y=*, src=*)
