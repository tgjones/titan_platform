from sdl2.SDL2_ttf cimport *
from titan.core.sdl.error cimport sdl_assert
from titan.core.sdl.subsystems.util.surface cimport Surface

import os

cdef class Font:

    def __init__(self, str path, int size=12, tuple color=(255, 255, 255)):

        sdl_assert(TTF_WasInit())

        if not os.path.isfile(path):
            raise IOError('Font not found: %s' % path)

        self.size = size
        self.color = color

        self.sdl_color.r = color[0]
        self.sdl_color.g = color[1]
        self.sdl_color.b = color[2]

        self.sdl_font = TTF_OpenFont(bytes(path, 'utf-8'), size)
        sdl_assert(self.sdl_font != NULL)

    def __dealloc__(self):
        TTF_CloseFont(self.sdl_font)

    cpdef print_to_surface(self, msg):
        msg = bytes(msg, 'utf-8')
        sdl_surface = TTF_RenderText_Blended(self.sdl_font, msg, self.sdl_color)
        cdef Surface surf = Surface(width=sdl_surface.w, height=sdl_surface.h)
        surf.sdl_surf = sdl_surface
        return surf
