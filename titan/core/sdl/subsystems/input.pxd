from titan.core.sdl.sdl_backend cimport SDLBackend

cdef class InputSubsystem:

    cdef SDLBackend backend
    cdef dict controllers
    cdef dict callback_map
