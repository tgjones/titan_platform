from cpython.pycapsule cimport *

from sdl2.SDL2 cimport *
from sdl2.SDL2_image cimport *
from sdl2.SDL2_mixer cimport *
from sdl2.SDL2_ttf cimport *
from titan.core.sdl.subsystems.display cimport DisplaySubsystem
from titan.core.sdl.subsystems.audio cimport AudioSubsystem
from titan.core.sdl.subsystems.input cimport InputSubsystem
from titan.core.sdl.error cimport sdl_assert, sdl_check

import logging as log
from titan.core import EventDispatcher

cdef class SDLBackend:

    def __init__(self, game, config):
        self.game = game
        self.running = False

        # Initialize SDL
        log.info('Initializing Simple DirectMedia Layer (SDL) backend')
        sdl_check(SDL_Init(SDL_INIT_EVERYTHING))

        # Initialize SDL_image
        flags = IMG_INIT_JPG | IMG_INIT_PNG
        sdl_assert(IMG_Init(flags) == flags)

        # Initialize SDL_ttf
        sdl_check(TTF_Init())

        # Graphics, audio and event subsystems
        self.display = DisplaySubsystem(self, **config['display'])
        self.audio = AudioSubsystem(self, **config['audio'])
        self.input = InputSubsystem(self)

        # Trigger startup hook
        EventDispatcher().trigger_event('backend_startup')

    def __del__(self):

        # Trigger backend shutdown hook
        EventDispatcher().trigger_event('backend_shutdown')

    def __dealloc__(self):

        # Shut down SDL libraries
        Mix_Quit()
        TTF_Quit()
        IMG_Quit()
        SDL_Quit()

    cpdef int get_time(self):
        return SDL_GetTicks()

    cpdef delay(self, int ticks):
        with nogil:
            SDL_Delay(ticks)

    def sys_message(self, msg, debug=False):

        log.debug('(sys_message): ' + msg)

        if debug and __debug__ or not debug:
            self.display.sys_messages.append((SDL_GetTicks(), msg))

    def _game_loop(self):

        # Trigger game_loop_begin event callbacks
        EventDispatcher().trigger_event('game_loop_begin')

        # Loop variables
        self.running = True
        cdef int remaining_ticks = 0

        # Game loop
        while True:
            start_ticks = SDL_GetTicks()

            # Detect and handle input events
            self.input.process_queue()

            # Shutdown flag set
            if not self.running:

                # Subsystem clearup
                self.audio.free_finished(force=True)
                self.input.disconnect_controllers()

                break

            # Trigger frame_begin event callbacks
            EventDispatcher().trigger_event('frame_begin', start_ticks=start_ticks)

            # Render frame
            self.display.render()

            # Trigger frame_end event callbacks
            elapsed_ticks = SDL_GetTicks() - start_ticks
            EventDispatcher().trigger_event('frame_end', elapsed_ticks=elapsed_ticks)

            # Free finished audio
            self.audio.free_finished()

            # Frame rate delay
            elapsed_ticks = SDL_GetTicks() - start_ticks
            remaining_ticks = 1000 / self.game._system['target_fps'] > elapsed_ticks
            if remaining_ticks > 0:
                with nogil:
                    SDL_Delay(remaining_ticks)

        # Trigger game_loop_end event callbacks
        EventDispatcher().trigger_event('game_loop_end')
