from titan.util.patterns import Singleton

class EventDispatcher(metaclass=Singleton):

    def __init__(self):
        self._events = {}

    def register_callback(self, hook, callback):
        if hook not in self._events:
            self._events[hook] = []
        self._events[hook].append(callback)

    def deregister_callback(self, hook, callback):
        if hook not in self._events:
            raise AttributeError('Event hook \'%s\' does not exist', hook)
        self._events[hook] = \
            [cb for cb in self._events[hook] if cb is not callback]

    def trigger_event(self, hook, *args, **kwargs):
        if hook in self._events:
            for cb in self._events[hook]:
                cb(*args, hook=hook, **kwargs)
