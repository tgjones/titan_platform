import os
import sys
import json
import logging
from copy import deepcopy
from functools import partial
from titan.core.backend import Backend
from titan.core.dispatcher import EventDispatcher


class Game:

    def __init__(self, config):

        # Merge system default config WITHOUT overriding
        sys_config_file = os.path.dirname(os.path.abspath(__file__)) + \
            os.sep + 'defaults.json'
        if os.path.isfile(sys_config_file):
            sys_config = json.load(open(sys_config_file, 'r'))
            config = _merge_config(sys_config, config)

        # Save/initialize attributes
        self._title = config['title']
        self._system = config['system']
        self._scenes = config['scenes']
        self._callbacks = config['callbacks']

        # Load logging config file
        if 'log_config' in self._system:
            logging.config.fileConfig(self.system['log_config'])

        # Otherwise use default logging settings
        else:

            # Setup root logger
            log = logging.getLogger()
            log.setLevel(logging.DEBUG if __debug__ else logging.INFO)

            # Setup default formatter
            fmt = logging.Formatter('%(levelname)-4.4s %(asctime)s '
                                    '%(threadName)-10.10s %(message)s',
                                    datefmt='%H:%M:%S')

            # Console log stream
            ch = logging.StreamHandler(sys.stdout)
            ch.setLevel(logging.DEBUG if __debug__ else logging.INFO)
            ch.setFormatter(fmt)
            log.addHandler(ch)

        # Map callbacks to methods in subclass
        if 'callbacks' in config:
            for callback in config['callbacks'].values():
                callback['handle'] = getattr(self, callback['call'])

        # Init backend singleton
        Backend(self, self._system)

        # Load first scene
        self.change_scene(config['first_scene'] \
            if 'first_scene' in config else 'main')

    def __repr__(self):
        return '<{}.{} object: current_scene={}>' \
            .format(__class__.__module__, __class__.__name__,
                    self.current_scene)

    @classmethod
    def from_json(cls, json_file):

        # Make sure config directory is absolute
        if not os.path.isabs(json_file):
            json_file = os.getcwd() + os.sep + json_file

        # Normalize config path (remove '..' and '.' etc)
        json_file = os.path.normpath(json_file)

        # Add config path to PYTHONPATH and set as working directory
        json_dir = os.path.dirname(json_file)
        sys.path.append(json_dir)
        os.chdir(json_dir)

        # Read config from JSON file
        config = json.load(open(json_file, 'r'))

        # Add custom PYTHONPATHs
        if 'system' in config and 'pythonpath' in config['system']:
            for path in config['system']['pythonpath']:
                if path not in sys.path:
                    sys.path.append(os.path.abspath(path))

        # Recursively merge in included config files
        if 'includes' in config:
            for include_file in config['includes']:
                include_config = parse_json_config(include_file)
                config = _merge_config(config, include_config)
            del config['includes']

        return cls(config)

    @property
    def title(self):
        return self._title

    @property
    def current_scene(self):
        return self._scenes[self._current_scene]

    def start_game_loop(self):
        Backend()._game_loop()

    def change_scene(self, new_scene):
        self._current_scene = new_scene

        for event in self.current_scene['events']:
            for hook in event['hooks']:
                for callback in event['callbacks']:

                    callback = self._callbacks[callback]
                    handle = callback['handle']

                    # Bind in custom arguments if specified
                    if 'args' in callback:
                        handle = partial(handle, *callback['args'])
                    if 'kwargs' in callback:
                        handle = partial(handle, **callback['kwargs'])

                    EventDispatcher().register_callback(hook, handle)

        # Trigger scene_change event callbacks
        EventDispatcher().trigger_event('scene_change', scene=new_scene)


def _merge_config(config_a, config_b):
    """ Recursively merges config_a into config_b and returns a copy."""

    # Merge config dictionaries
    if isinstance(config_b, dict):
        config_a = deepcopy(config_a)
        for k, v in config_b.items():
            if k in config_a:
                config_a[k] = _merge_config(config_a[k], v)
            else:
                config_a[k] = v
        return config_a

    # Merge config list
    elif isinstance(config_b, list):
        config_a = deepcopy(config_a)
        for v in config_b:
            if v in config_a:
                config_a = _merge_config(config_a[config_a.index(v)], v)
            else:
                config_a.append(v)
        return config_a

    # Config leaf node
    else:
        return config_b
