from titan.util.patterns import Singleton

class Backend(metaclass=Singleton):

    def __init__(self, game, config):
        self._backend_id = config['backend'].lower()

        if self._backend_id == 'sdl' or self._backend_id == 'sdl2':

            # Load backend controller
            from titan.core.sdl.sdl_backend import SDLBackend
            self._impl = SDLBackend(game, config)

        else:
            raise SystemError('Unsupported backend: ' + backend)

    def __call__(self, *args, **kwargs):
        return self._impl
