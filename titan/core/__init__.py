from titan.core.game import Game
from titan.core.backend import Backend
from titan.core.dispatcher import EventDispatcher

import pkg_resources as pkg
__version__ = pkg.require("Titan-Platform")[0].version
