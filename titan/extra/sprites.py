import os
import json
from pprint import pformat
from titan.core import Backend
from titan.util.types import Rect


class SpriteSheet:

    _textures = {}

    def __init__(self, path, frames={}, color_key=None):
        self._path = os.path.abspath(path)
        self._frames = frames

        if path not in self._textures:

            # Load texture in renderer (use path as ID)
            gfx = Backend().display
            surf = gfx.create_surface_from_image(self._path,
                                                 color_key=color_key)
            Backend().display.load_texture(surf, self._path)

            # Save texture reference counter
            self._textures[self._path] = 1

        else:

            # Increment reference counter
            self._textures[self._path] += 1

    def __del__(self):

        # Decrement reference counter
        self._textures[self._path] -= 1

        # Unload texture from renderer if ref count is 0
        if self._textures[self._path] == 0:
            Backend().display.unload_texture(self._path)
            del self._textures[self._path]

    def __repr__(self):
        return '<{}.{} object: path={}\n frames={}>' \
            .format(__class__.__module__, __class__.__name__, self._path,
                pformat(self._frames, indent=2))

    @property
    def path(self):
        return self._path

    @property
    def frames(self):
        return self._frames

    @classmethod
    def from_tp_json(cls, json_file, **kwargs):

        atlas = json.load(open(json_file, 'r'))

        frames = {}
        for id_, frame in atlas['frames'].items():
            frames[id_] = Rect(int(frame['frame']['x']),
                               int(frame['frame']['y']),
                               int(frame['frame']['w']),
                               int(frame['frame']['h']));

        return cls(atlas['meta']['image'], frames, **kwargs)


class Sprite(Rect):

    _sprites = set()

    def __init__(self, sheet, x=0, y=0, z=0, animations=None, frame=None):
        self._sprites.add(self)

        # Load sprite sheet directly from image file
        if isinstance(sheet, str):
            self._sheet = SpriteSheet(sheet)

        # Otherwise assume SpriteSheet was loaded elsewhere
        else:
            self._sheet = sheet

        # Load initial frame
        u = 0; v = 0; w = 0; h = 0
        if frame is not None:
            u = self._sheet.frames[frame].x
            v = self._sheet.frames[frame].y
            w = self._sheet.frames[frame].width
            h = self._sheet.frames[frame].height

        # Create render entity
        self._gfx = Backend().display
        self._gfx.create_entity(id(self), x, y, z, w, h, u, v,
            texture_id=self._sheet.path)

        w, h = self._gfx.entity_size(id(self))
        super().__init__(x, y, w, h)
        self.place(x, y)
        self.resize(w, h)

    def __del__(self):
        self._sprites.remove(self)

    def __repr__(self):
        return '<{}.{} object: sheet={}, render_id={} x={}, y={}, w={}, h={}>' \
            .format(__class__.__module__, __class__.__name__, self._sheet,
                    self._render_id, self._x, self._y, self._w, self._h)

    def __hash__(self):
        return hash(id(self))

    def __eq__(self, other):
        return self is other

    def __ne__(self, other):
        return self is not other

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @x.setter
    def x(self, value):
        self._x = value
        self.place(self._x, self._y)

    @y.setter
    def y(self, value):
        self._y = value
        self.place(self._x, self._y)

    def place(self, x, y):
        self._x = x
        self._y = y
        self._gfx.place_entity(id(self), x, y)

    def move(self, x, y):
        self._x += x
        self._y += y
        self._gfx.move_entity(id(self), x, y)

    def resize(self, w, h):
        self._w = w
        self._h = h
        self._gfx.resize_entity(id(self), w, h)
