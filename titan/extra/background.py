from titan.core import Backend

def draw_background(path, tile=False):

    gfx = Backend().display

    # Create background surface to draw to
    back_surf = gfx.create_surface(gfx.viewport.width, gfx.viewport.height)

    # Load background image from disk
    image = gfx.create_surface_from_image(path)

    if tile:
        for x, y in [(x, y) for x in range(0, back_surf.width, image.width) \
                            for y in range(0, back_surf.height, image.height)]:
            back_surf.blit(image, x, y)

    else:
        back_surf.blit(image)

    # Load into renderer
    gfx.load_texture(back_surf, '__background__')
    gfx.create_entity('__background__')
