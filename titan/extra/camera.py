from titan.core import Backend
from titan.util.patterns import Singleton

class Camera(metaclass=Singleton):

    def __init__(self, x=0, y=0):
        self.place(x, y)

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @x.setter
    def x(self, value):
        self._x = value
        self.place(value, self._y)

    @y.setter
    def y(self, value):
        self._y = value
        self.place(self._x, value)

    def place(self, x, y):
        self._x = x
        self._y = y
        Backend().display.place_viewport(x, y)

    def move(self, x, y):
        self._x += x
        self._y += y
        Backend().display.move_viewport(x, y)
