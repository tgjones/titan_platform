from xml.etree import ElementTree as ET
from titan.core import Backend
from titan.extra import Hitbox
from titan.util.types import Point, Rect
import base64
import struct
import zlib


class TmxParseError(RuntimeError):
    pass


class TmxRenderError(RuntimeError):
    pass


class TileSet:

    def __init__(self, name, path, first_gid, width, height,
                 tile_width, tile_height):
        self._name = name
        self._path = path
        self._first_gid = first_gid
        self._width = width
        self._height = height
        self._tile_width = tile_width
        self._tile_height = tile_height
        self._source = Backend().display.create_surface_from_image(self._path)

    @property
    def name(self):
        return self._name

    @property
    def path(self):
        return self._path

    @property
    def first_gid(self):
        return self._first_gid

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def tile_width(self):
        return self._tile_width

    @property
    def tile_height(self):
        return self._tile_height

    @property
    def source(self):
        return self._source


class Layer:

    def __init__(self, name, width, height, tile_width, tile_height,):
        self._name = name
        self._width = width
        self._height = height
        self._tile_width = tile_width
        self._tile_height = tile_height
        self._properties = {}
        self._gids = []
        self._hitboxes = []

        self._surface = Backend().display.create_surface(
            self._width * self._tile_width, self._height * self._tile_height)

    @property
    def name(self):
        return self._name

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def tile_width(self):
        return self._tile_width

    @property
    def tile_height(self):
        return self._tile_height

    @property
    def properties(self):
        return self._properties

    def decode_b64(self, data, compression=None):

        # Decode and optionally decompress
        arr = base64.b64decode(data)
        if compression == 'zlib' or compression == 'gzip':
            arr = zlib.decompress(arr)
        elif compression is not None:
            raise TmxParseError('Unsupported data compression method: \'%s\''
                                % compression)

        # Split array into four byte sections and convert then into 32-bit
        # little-endian ordered unsigned integers
        arr = [arr[i:i + 4] for i in range(0, len(arr), 4)]
        self._gids = [struct.unpack('<L', gid)[0] for gid in arr]

    def generate(self, tile_sets):

        if len(self._gids) == 0:
            raise TmxRenderError('No GIDs to draw')

        # Derefence tile sets and draw tiles
        for gid, loc in zip(self._gids, range(0, len(self._gids))):

            # Skip empty tiles
            if gid == 0:
                continue

            # Find tileset GID is from
            for ts in tile_sets:
                if gid > ts.first_gid:
                    break

            # Calculate source (tile set) pixel coordinates
            idx = (gid - 1) - (ts.first_gid - 1)
            src_tile_x = idx * ts.tile_width % ts.width
            src_tile_y = idx * ts.tile_width // ts.width * ts.tile_height

            # Calculate destination (layer) grid reference
            dst_tile_x = loc % self._width * self._tile_width
            dst_tile_y = loc // self._width * self._tile_height

            # Blit tile over to layer
            self._surface.blit(ts.source, dst_tile_x, dst_tile_y,
                               Rect(x=src_tile_x,
                                    y=src_tile_y,
                                    w=ts.tile_width,
                                    h=ts.tile_height))

            # Create hitboxes if collision is enabled for layer
            if 'collision' in self._properties and \
                self._properties['collision'].lower() == 'true':
                self._hitboxes.append(Hitbox(dst_tile_x, dst_tile_y,
                                             ts.tile_width, ts.tile_height))

        # Load layer into renderer and create graphics entity
        id_ = '__tmx_layer_' + self._name + '__'
        gfx = Backend().display
        gfx.load_texture(self._surface, id_)
        gfx.create_entity(id_)


class TmxMap:

    def __init__(self, path):
        self._path = path
        self._tile_sets = []
        self._layers = []

        tree = ET.parse(self._path)
        root = tree.getroot()

        self._width = int(root.attrib['width'])
        self._height = int(root.attrib['height'])
        self._tile_width = int(root.attrib['tilewidth'])
        self._tile_height = int(root.attrib['tileheight'])

        # Parse tile sets and index by first GID
        for child in root.findall('tileset'):
            ts = TileSet(name=child.attrib['name'],
                         path=child.find('image').attrib['source'],
                         first_gid=int(child.attrib['firstgid']),
                         width=int(child.find('image').attrib['width']),
                         height=int(child.find('image').attrib['height']),
                         tile_width=int(child.attrib['tilewidth']),
                         tile_height=int(child.attrib['tileheight']))
            self._tile_sets.append(ts)

        # Parse and draw layers
        for child in root.findall('layer'):
            layer = Layer(name=child.attrib['name'],
                          width=self._width,
                          height=self._height,
                          tile_width=self._tile_width,
                          tile_height=self._tile_height)
            self._layers.append(layer)

            # Base64 encoded layer data
            data = child.find('data')
            if data.attrib['encoding'] == 'base64':

                # Decode and optionally decompress
                layer.decode_b64(data.text, data.attrib['compression'])

            else:
                raise TmxParseError('Unsupported data encoding method: \'%s\''
                                    % child.attrib['encoding'])

            # Parse layer properties
            props = child.find('properties')
            if props is not None:
                for prop in props.findall('property'):
                    layer.properties[prop.attrib['name']] = \
                        prop.attrib['value']

            # Draw layer to surface and create hitboxes
            layer.generate(self._tile_sets)

    @property
    def width(self):
        return self._width * self._tile_width

    @property
    def height(self):
        return self._height * self._tile_height

    @property
    def collision_zone(self):
        return self._collision_zone
