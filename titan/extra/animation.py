import json
from webcolors import hex_to_rgb
from titan.core import Backend, EventDispatcher
from titan.extra import Sprite, SpriteSheet

class AnimatedSprite(Sprite):

    def __init__(self, animations, **kwargs):
        self._animations = animations
        self._playing_animation = None
        super().__init__(
            frame=next(iter(self._animations.values()))[0][0], **kwargs)

    @classmethod
    def from_json(cls, json_file, **kwargs):
        config = json.load(open(json_file, 'r'))

        # Get sprite sheet
        sheet = SpriteSheet.from_tp_json(config['sheet'], color_key=
            hex_to_rgb(config['color_key']) if 'color_key' in config else None)

        # Setup animation frames
        animations = {}
        for k, v in config['animations'].items():
            animations[k] = []
            for frm in v:
                animations[k].append((frm['frame'], frm['duration']))

        return cls(sheet=sheet, animations=animations, **kwargs)

    def start_animation(self, animation, speed=1, loop=False):
        if animation not in self._animations:
            raise ValueError('No animation \'%s\'' % animation)

        self._playing_animation = self._animations[animation]
        self._start_time = Backend().get_time()
        self._looping = loop

        EventDispatcher().register_callback('frame_begin',
            self._update_animation)

    def stop_animation(self):
        self._playing_animation = None
        del self._start_time, self._looping

        EventDispatcher().deregister_callback('frame_begin',
            self._update_animation)

    def _update_animation(self, *args, **kwargs):
        if self._playing_animation is None:
            return

        time = Backend().get_time()
        elapsed = time - self._start_time

        # Get current frame
        pos = 0
        for frame in self._playing_animation:
            pos += frame[1] # duration
            if pos > elapsed:
                break

        # At end of animation stop or loop
        if elapsed > pos:
            if self._looping:
                self._start_time = time
            else:
                self.stop_animation()
            return

        # Get frame coordinates within sheet
        u = self._sheet.frames[frame[0]].x
        v = self._sheet.frames[frame[0]].y

        # Set frame on texture
        Backend().display.frame_entity(id(self), u, v)
