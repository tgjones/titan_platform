from titan.core import Backend
from titan.extra import Sprite
from titan.util.types import Rect

class Hitbox(Rect):

    _hitboxes = set()

    def __init__(self, x, y, w, h, debug=False):
        super().__init__(x, y, w, h)
        self._debug = debug
        self._hitboxes.add(self)

        # Debug rendering
        if self._debug:
            gfx = Engine().graphics
            surf = gfx.create_surface(self._w, self._h)
            surf.fill(255, 0, 0, 128)
            gfx.load_texture(surf, id(self))
            gfx.create_entity(self._render_id, x=self._x, y=self._y, z=-10000)

    def __del__(self):
        self._hitboxes.remove(self)

        # Debug rendering
        if self._debug:
            gfx = Engine().graphics
            gfx.destroy_entity(id(self))
            gfx.unload_texture(id(self))

    def __repr__(self):
        return '<{}.{} object: x={}, y={}, w={}, h={}>' \
            .format(__class__.__module__, __class__.__name__,
                    self._x, self._y, self._w, self._h)

    def __hash__(self):
        return hash(id(self))

    def __eq__(self, other):
        return self is other

    def __ne__(self, other):
        return self is not other

    def collided_with(self, other):
        return self._x < other.x + other.width and \
            self._x + self._w > other.x and \
            self._y < other.y + other.height and \
            self._y + self._h > other.y

    def try_move(self, x, y):

        other_hitboxes = [hb for hb in self._hitboxes if hb is not self]

        old_loc = Hitbox(self._x, self._y, self._w, self._h)
        new_loc = Hitbox(self._x + x, self._y + y, self._w, self._h)

        # Clip new location based on other hitboxes
        hits = []
        for hitbox in other_hitboxes:

            # Detect a collision
            if new_loc.collided_with(hitbox):

                # Save record of collision
                hits.append(hitbox)

                # Check left edge
                if old_loc.x >= hitbox.x + hitbox.width and \
                    new_loc.x < hitbox.x + hitbox.width:
                    new_loc.x = hitbox.x + hitbox.width

                # Check right edge
                if old_loc.x + old_loc.width <= hitbox.x and \
                    new_loc.x + new_loc.width > hitbox.x:
                    new_loc.x = hitbox.x - new_loc.width

                # Check top edge
                if old_loc.y >= hitbox.y + hitbox.height and \
                    new_loc.y < hitbox.y + hitbox.height:
                    new_loc.y = hitbox.y + hitbox.height

                # Check bottom edge
                if old_loc.y + old_loc.height <= hitbox.y and \
                    new_loc.y + new_loc.height > hitbox.y:
                    new_loc.y = hitbox.y - new_loc.height

        # Move hitbox to updated location
        self._x = new_loc.x
        self._y = new_loc.y

        # Debug rendering
        if self._debug:
            Backend().display.place_entity(id(self), new_loc.x, new_loc.y)

        # Return adjusted movement vector
        return new_loc.x - old_loc.x, new_loc.y - old_loc.y, hits


class CollidableSprite(Sprite):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._hitbox = Hitbox(self._x, self._y, self._w, self._h)

    # Override movement to check for collision
    def move(self, x, y):
        x, y, hits = self._hitbox.try_move(x, y)
        super().move(x, y)
        return x, y, hits
