MacOS X
=======

Titan supports 'Vanilla' or 'MacPorts' implementations of Python and the pre-requisite packages and installation varies
accordingly.  Subsequent Titan-specific setup steps are identical.  Using the MacPorts installation is arguably more
'bleeding edge' as package versioning is controlled by the package manager, so if stability is more important that the
latest technology to you, this is not the installation type for you.

Unlike Windows, packages on OS X tend to come bundled with both 32-bit and 64-bit versions, so that is generally not a
concern on this platform.  Also it is worth noting that OS X ships with a bespoke build of the Python interpreter, Titan
will *not* work under this version of the interpreter.

Pre-requisites
--------------

Vanilla
~~~~~~~

* `Python 3.4.1 <https://www.python.org/ftp/python/3.4.1/python-3.4.1-macosx10.6.dmg>`_
* `SDL 2.0.3 <https://www.libsdl.org/release/SDL2-2.0.3.dmg>`_
* `SDL_mixer 2.0.0 <https://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-2.0.0.dmg>`_
* `SDL_image 2.0.0 <https://www.libsdl.org/projects/SDL_image/release/SDL2_image-2.0.0.dmg>`_
* `SDL_ttf 2.0.12 <https://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-2.0.12.dmg>`_
* XCode command line tools (see below)

To install the XCode command line tools on OS X 10.9 Mavericks, open the Terminal app and type the command
``xcode-select --install`` and press the 'Install' button on the dialog box that pops up.

MacPorts
~~~~~~~~

* `MacPorts <https://distfiles.macports.org/MacPorts/>`_
* Python 3 ('python3', 'py3-setuptools', 'py3-pip' and 'py3-virtualenv' packages)
* SDL 2 ('sdl2' package, this includes SDL_image et al)
* GNU C Compiler ('gcc' package)

With MacPorts installed and with ``/opt/local/bin`` added to you ``$PATH`` environment variable , open the Terminal
app and type the following commands to first update MacPorts, and then install the packages listed above (some of the
packages are implied as dependencies):

    sudo port selfupdate
    sudo port install python3 py3-pip py3-virtualenv sdl2

Virtual Environment Setup
-------------------------

To setup the virtual environment on OS X you must initially follow steps that are dependent on if you have chosen to use
the Vanilla or MacPorts strategy.  The latter steps are shared between both, and after the initial setup is complete it
is not necessary to worry about which type of installation you are using.

These steps have been tested on OS X Mavericks and your mileage on previous version may vary.

Vanilla Only
~~~~~~~~~~~~

.. todo:: Write this section

MacPorts Only
~~~~~~~~~~~~~

.. todo:: Write this section

Both Vanilla and MacPorts
~~~~~~~~~~~~~~~~~~~~~~~~~

.. todo:: Write this section

Building Cython Modules
-----------------------

Building the Cython modules is the same on Vanilla and MacPorts installations:

1. Open the Terminal app and cd into your cloned repository (``cd ~/workspace/titan_platform`` for example)
2. Enter the virtual environment with the command ``. venv/bin/activate``
3. Build the Cython modules with the command ``python setup.py build_ext --inplace``
