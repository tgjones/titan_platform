Windows
=======

Python 32-bit and 64-bit editions come packaged separately on Windows, and for simplicity and compatibility the 32-bit
version is used in Titan, this may change one day.  The following steps have been tested to work on Windows 7 64-bit,
but will likely work on other editions of Windows versions XP or later.

Pre-requisites
--------------

As mentioned above, please ensure you download and install the 32-bit versions of the following packages:

* `Microsoft Visual C++ Express 2010 <http://go.microsoft.com/?linkid=9709949>`_
* `Python 3.4.1 <https://www.python.org/ftp/python/3.4.1/python-3.4.1.msi>`_
* `SDL 2.0.3 <https://www.libsdl.org/release/SDL2-2.0.3-win32-x86.zip>`_
* `SDL_mixer 2.0.0 <https://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-2.0.0-win32-x86.zip>`_
* `SDL_image 2.0.0 <https://www.libsdl.org/projects/SDL_image/release/SDL2_image-2.0.0-win32-x86.zip>`_
* `SDL_ttf 2.0.12 <https://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-2.0.12-win32-x86.zip>`_

Visual C++ should be installed in the default location with the default settings, and the Python installation directory
(C:\Python34 is recommended) and the Scripts sub-directory should both be added to the PATH environment variable.  The
MinGW installer should be run and the base system and compiler should be installed to the default location (C:\MinGW).
Finally, the SDL packages should be extracted directly into the C:\ drive, so that SDL is extracted to C:\SDL2-x.x.x and
so on.

Virtual Environment Setup
-------------------------

Assuming the above pre-requisites are satisfied you may follow the following steps to configure your development
environment:

1. Using your favourite Git client (`TortoiseGit <https://code.google.com/p/tortoisegit/>`_ recommended) clone the Titan
   platform to a working directory of choice (C:\Workspace for example)
2. Open a command prompt (Win+R, cmd, Enter) and cd into your cloned repository (``cd C:\Workspace\titan_platform``
   for example)
3. Install virtualenv with the command ``pip install virtualenv``
4. Create a local virtual environment with the command ``virtualenv venv``, by using the name 'venv' the .gitignore
   file will prevent accidental adding of the virtual environment files to the Git repository
5. Edit the activate.bat file with the command ``notepad venv\Scripts\activate.bat``
6. Right above the ``:END`` line in the file add the following lines (assuming you used the installation directories
   recommended):

        set "PYTHONPATH=%VIRTUAL_ENV%\.."
        set "PATH=%VIRTUAL_ENV%\Scripts;C:\SDL2-2.0.3\lib\x86;C:\SDL2_mixer-2.0.0\lib\x86;C:\SDL2_image-2.0.0\lib\x86;C:\SDL2_ttf-2.0.0\lib\x86;%PATH%"

7. Quit Notepad and save the file
8. Enter the virtual environment with the command ``venv\Scripts\activate``
9. Download Python dependencies, including Cython, with the command ``pip install -r requirements.txt``

Building Cython Modules
-----------------------

With the above setup complete, follow these steps to build the Cython modules within the source:

1. Open a command prompt (Win+R, cmd, Enter) and cd into your cloned repository (``cd C:\Workspace\titan_platform``
   for example)
2. Enter the virtual environment with the command ``venv\Scripts\activate``
3. Build the Cython modules with the command ``python setup.py build_ext --inplace``

Optional MinGW Compiler
-----------------------

As an alternative to the Microsoft Visual C++ (MSVC) Compiler, the GNU-style MinGW compiler may instead be used.  This
may be necessary if you get build errors when installing Cython and building the Cython modules (below).  To install
MinGW you may use the `installer <http://downloads.sourceforge.net/project/mingw/Installer/mingw-get-setup.exe>`_ to get
the latest version, and then follow the following steps to set up you virtual environment to use MinGW instead of MSVC:

1. Open a command prompt (Win+R, cmd, Enter) and cd into your cloned repository (``cd C:\Workspace\titan_platform``
   for example)
2. Enter the virtual environment with the command ``venv\Scripts\activate``
3. Edit the distutils.cfg file with the command ``notepad venv\Lib\distutils\distutils.cfg``
4. At the end of the file add the following lines:

        [build]
        compiler=mingw32

5. Quit Notepad and save the file
