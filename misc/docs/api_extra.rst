Extra Libraries
===============

Game Sprites
------------

.. automodule:: titan.extra.sprites

Animations
----------

.. automodule:: titan.extra.animation

Tiled Map Format (TMX)
----------------------

.. automodule:: titan.extra.tiled

Collision Detection
-------------------

.. automodule:: titan.extra.collisions

Camera Handling
---------------

.. automodule:: titan.extra.camera

Background
----------

.. automodule:: titan.extra.background
