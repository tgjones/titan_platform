Event Hooks
===========

In Titan the primary means of customising functionality is by binding custom callbacks to 'event hooks', event hooks are
arbitrary strings that are triggered by certain events within the system, be it user input or some other means.

When triggered, any callbacks that have been bound to the given hook are called (in an undefined order), with the
current state of the Game instance as the first parameter, and additional data pertaining to the event are passed into
the callback as keyword arguments.

Callbacks generally have the signature ``def callback(game, args, kwargs)`` and by default ``len(args) == 0``,
and ``len(kwargs) >= 1``.  The system will always pass the ``hook`` keyword argument containing the hook that
triggered the callback, and many hooks will also pass the ``meta`` keyword argument which contains data specifying a
more granular state for the event, such as a key state 'up' or 'down'.

Custom positional and keyword arguments may be specified in the callback stanza of your game configuration, for example:

    "callbacks": {
        "spawn": {
            "call": "spawn_bunnies",
            "args": [ 50 ],
            "kwargs": { "speed": 100 }
        },
    }

This following tables enumerates the available hooks, their triggers and the keyword arguments passed to callbacks.

System
------

.. csv-table::
   :header: "Hook", "Trigger Event", "Keyword Arguments"
   :stub-columns: 1
   :delim: |

   backend_startup | Backend has been initialised | none
   backend_shutdown | Backend has been deallocated | none
   window_shown | Application window was explicitly shown on-screen | none
   window_hidden  | Application window was explicitly hidden from view | none
   window_exposed | Application window was exposed | none
   window_moved | Application window was moved | x=new x location, y=new y location
   window_resized | Application window was explicitly resized | width=new width, height=new height
   window_minimized | Application window was minimized | none
   window_maximized | Application window was maximized | none
   window_restored | Application window was restored | none
   window_focus | Application window gained input focus | meta='mouse' or 'keyboard'
   window_unfocus | Application window lost input focus | meta='mouse' or 'keyboard'
   window_close | Application window has been closed | none
   controller_connected | Game controller connected | id=controller identifier
   controller_disconnected | Game controller disconnected | id=controller identifier

Game Loop
---------

.. csv-table::
   :header: "Hook", "Trigger Event", "Keyword Arguments"
   :stub-columns: 1
   :delim: |

   game_loop_begin | Before the game loop has started | none
   frame_begin | Before any event processing | start_ticks=ticks since start of program execution
   scene_change | The game scene has just changed | scene=identifier of new scene
   render_begin | After event processing but before rendering | none
   render_end | After rendering and event processing | none
   frame_end | After rendering and before frame delay | elapsed_ticks=ticks since frame_start
   game_loop_end | After the game loop has exited | none

Controller Input
----------------

.. csv-table::
   :header: "Hook", "Trigger Event", "Keyword Arguments"
   :stub-columns: 1
   :delim: |

   controller_l_axis | Left stick motion | meta='x' or 'y', value=axis position, id=controller identifier
   controller_r_axis | Right stick motion | meta='x' or 'y', value=axis position, id=controller identifier
   controller_l_trigger | Left trigger motion | value=axis position, id=controller identifier
   controller_r_trigger | Right trigger motion | value=axis position, id=controller identifier
   controller_btn_a | A button state change | meta='up' or 'down', id=controller identifier
   controller_btn_b | B button state change | meta='up' or 'down', id=controller identifier
   controller_btn_x | X button state change | meta='up' or 'down', id=controller identifier
   controller_btn_y | Y button state change | meta='up' or 'down', id=controller identifier
   controller_btn_back | Back button state change | meta='up' or 'down', id=controller identifier
   controller_btn_guide | Guide button state change | meta='up' or 'down', id=controller identifier
   controller_btn_start | Start button state change | meta='up' or 'down', id=controller identifier
   controller_btn_l_stick | Left stick button state change | meta='up' or 'down', id=controller identifier
   controller_btn_r_stick | Right stick button state change | meta='up' or 'down', id=controller identifier
   controller_l_bumper | Left bumper state change | meta='up' or 'down', id=controller identifier
   controller_r_bumper | Right bumper state change | meta='up' or 'down', id=controller identifier
   controller_dpad_up | Directional pad up state change | meta='up' or 'down', id=controller identifier
   controller_dpad_down | Directional pad down state change | meta='up' or 'down', id=controller identifier
   controller_dpad_left | Directional pad left state change | meta='up' or 'down', id=controller identifier
   controller_dpad_right | Directional pad right state change | meta='up' or 'down', id=controller identifier

Touch Input
-----------

.. csv-table::
   :header: "Hook", "Trigger Event", "Keyword Arguments"
   :stub-columns: 1
   :delim: |

   finger_down | Finger touch | meta=finger index, x=x location (0..1), y=y location (0..1), pressure=pressure (0..1)
   finger_up | Finger remove | meta=finger index, x=x location (0..1), y=y location (0..1),
   finger_move | Finger motion | meta=finger index, x=x location (0..1), y=y location, dx=x movement (0..1), dy=y movement (0..1)

Mouse Input
-----------

.. csv-table::
   :header: "Hook", "Trigger Event", "Keyword Arguments"
   :stub-columns: 1
   :delim: |

   mouse_move | Mouse motion | x=new x position, y=new y position, xrel=x change since last mouse_move, yrel=y change since last mouse_move
   mouse_btn_1 | Left mouse button state change | meta='up' or 'down', x=x position, y=y position
   mouse_btn_2 | Right mouse button state change | meta='up' or 'down', x=x position, y=y position
   mouse_btn_3 | Middle mouse button state change | meta='up' or 'down', x=x position, y=y position
   mouse_btn_4 | Extra mouse button 1 state change | meta='up' or 'down', x=x position, y=y position
   mouse_btn_5 | Extra mouse button 2 state change | meta='up' or 'down', x=x position, y=y position
   mouse_wheel | Mouse wheel state change | x=new x value, y=new y value

Keyboard Input (Standard)
-------------------------


.. csv-table::
   :header: "Hook", "Trigger Event", "Keyword Arguments"
   :stub-columns: 1
   :delim: |

   key_enter | Enter key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_escape | Escape key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_backspace | Backspace key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_tab | Tab key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_spacebar | Spacebar key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_exclaim | Exclamation point (!) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_dbl_quote | Double quote (") key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_hash | Hash (#) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_percent | Percent (%) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_dollar | Dollar ($) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_ampersand | Ampersand (&) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_quote | Quote key (') state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_l_paren | Left parenthesis (() key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_r_paren | Right parenthesis ()) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_asterisk | Asterisk () key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_plus | Plus (+) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_comma | Comma (,) key state change | meta='up' or 'down', repeat=true if this is a repeat event    key_minus | Minus (-) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_period | Period (.) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_fwd_slash | Forward slash (/) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_back_slash | Back slash (\\) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_0 | 0 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_1 | 1 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_2 | 2 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_3 | 3 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_4 | 4 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_5 | 5 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_6 | 6 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_7 | 7 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_8 | 8 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_9 | 9 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_colon | Colon (:) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_semicolon | Semi colon (;) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_less | Less than (<) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_greater | Greater than (>) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_equals | Equals (=) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_question | Question mark (?) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_l_sq_bracket | Left square bracket ([) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_r_sq_bracket | Right square bracket (]) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_caret | Caret (^) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_underscore | Caret (_) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_backquote | Caret (`) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_caps_lock | Caps lock key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_a | A key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_b | B key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_c | C key state change | meta='up' or 'down', repeat=true if this is a repeat   key_d | D key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_e | E key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f | F key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_g | G key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_h | H key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_i | I key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_j | J key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_k | K key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_l | L key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_m | M key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_n | N key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_o | O key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_p | P key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_q | Q key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_r | R key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_s | S key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_t | T key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_u | U key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_v | V key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_w | W key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_x | X key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_y | Y key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_z | Z key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f1 | F1 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f2 | F2 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f3 | F3 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f4 | F4 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f5 | F5 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f6 | F6 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f7 | F7 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f8 | F8 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f9 | F9 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f10 | F10 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f11 | F11 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f12 | F12 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_print_screen | Print screen key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_scroll_lock | Scroll lock key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_pause | Pause key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_insert | Print screen key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_delete | Delete key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_home | Home key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_end | End key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_page_up | Page up key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_page_down | Page down key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_right | Right arrow key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_left | Left arrow key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_down | Down arrow key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_up | Up arrow key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_num_lock | Number lock key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_divide | Keypad divide (/) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_multiply | Keypad multiply () key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_minus | Keypad multiply (-) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_enter | Keypad enter key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_1 | Keypad 1 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_2 | Keypad 2 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_3 | Keypad 3 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_4 | Keypad 4 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_5 | Keypad 5 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_6 | Keypad 6 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_7 | Keypad 7 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_8 | Keypad 8 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_9 | Keypad 9 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_0 | Keypad 0 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_decimal | Keypad decimal point (.) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_equals | Keypad equals (=) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_power | Power key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_sleep | Sleep key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_eject | Eject key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_application | Application key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_l_ctrl | Left ctrl key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_l_shift | Left shift key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_l_alt | Left alt key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_l_meta | Left meta (Windows, cmd) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_r_ctrl | Right ctrl key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_r_shift | Right shift key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_r_alt | Right alt key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_r_meta | Right meta (Windows, cmd) key state change | meta='up' or 'down', repeat=true if this is

Keyboard Input (Media)
----------------------

.. csv-table::
   :header: "Hook", "Trigger Event", "Keyword Arguments"
   :stub-columns: 1
   :delim: |

   key_volume_up | Volume up key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_volume_down | Volume down key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_audio_next | Audio next key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_audio_prev | Audio previous key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_audio_stop | Audio stop key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_audio_play | Audio play key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_audio_mute | Audio mute key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_media_select | Media select key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_www | WWW key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_mail | Mail next key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_calculator | Calculator key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_audio_next | Audio next key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_ac_search | Application control search key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_ac_home | Application control home key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_ac_back | Application control back key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_ac_forward | Application control forward key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_ac_stop | Application control stop key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_ac_refresh | Application control refresh key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_ac_bookmarks | Application control bookmarks key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_brightness_down | Brightness down key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_brightness_up | Brightness up key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_display_switch | Display switch key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kbd_illum_down | Keyboard illumination down key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kbd_illum_up | Keyboard illumination up key state change | meta='up' or 'down', repeat=true if this is a repeat event

Keyboard Input (Miscellaneous)
------------------------------

.. csv-table::
   :header: "Hook", "Trigger Event", "Keyword Arguments"
   :stub-columns: 1
   :delim: |

   key_f13 | F13 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f14 | F14 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f15 | F15 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f16 | F16 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f17 | F17 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f18 | F18 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f19 | F19 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f20 | F20 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f21 | F21 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f22 | F22 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f23 | F23 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_f24 | F24 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_execute | Execute key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_help | Help key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_menu | Menu key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_select | Select key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_stop | Stop key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_again | Again key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_undo | Undo key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_cut | Cut key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_copy | Copy key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_paste | Paste key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_find | Find key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_alterase | Alt erase key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_sysreq | SysReq key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_cancel | Cancel key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_clear | Clear key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_return | Return key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_out | Out key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_oper | Oper key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_clear_again | Clear again key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_crsel | CrSel key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_exsel | ExSel key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_thousand_separator | Thousand separator key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_decimal_separator | Decimal separator key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_currency_unit | Currency unit key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_currency_subunit | Currency sub-unit key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_mode | Mode key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_00 | Keypad 00 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_000 | Keypad 000 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_comma | Keypad comma (,) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_equalsas400 | Equals AS400 key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_l_paren | Keypad left parenthesis (() key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_r_paren | Keypad right parenthesis ())key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_l_brace | Keypad left brace ({) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_r_brace | Keypad right brace (}) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_tab | Keypad tab key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kb_backspace | keypad backspace state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_a | Keypad A key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_b | Keypad B key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_c | Keypad C key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_d | Keypad D key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_e | Keypad E key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_f | Keypad F key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_xor | Keypad X-OR key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_power | Keypad power (^) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_percent | Keypad percent (%) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_less | Keypad less than (<) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_greater | Keypad greater than (>) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_ampersand | Keypad ampersand (&) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_dbl_ampersand | Keypad double ampersand (&&) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_vertical_bar | Keypad key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_dbl_vertical_bar | Keypad double vertical bar key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_colon | Keypad colon (:) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_hash | Keypad hash (#) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_space | Keypad space key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_at | Keypad at (@) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_exclamation | Keypad exclamation (!) key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_mem_store | Keypad memory store key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_mem_recall | Keypad memory recall key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_mem_clear | Keypad memory clear key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_mem_add | Keypad memory add key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_mem_subtract | Keypad memory subtract key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_mem_multiply | Keypad memory multiply key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_mem_divide | Keypad memory divide key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_plus_minus | Keypad plus or minus key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_clear | Keypad clear key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_clear_entry | Keypad clear entry key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_binary | Keypad binary key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_octal | Keypad octal key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_decimal | Keypad decimal key state change | meta='up' or 'down', repeat=true if this is a repeat event
   key_kp_hexadecimal | Keypad hexadecimal key state change | meta='up' or 'down', repeat=true if this is a repeat event
