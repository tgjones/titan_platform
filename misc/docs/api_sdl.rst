SDL Backend
===========

Facade
------

.. automodule:: titan.core.sdl.sdl_backend

Subsystems
----------

.. automodule:: titan.core.sdl.display
.. automodule:: titan.core.sdl.audio
.. automodule:: titan.core.sdl.input

Utility Classes
---------------

.. automodule:: titan.core.sdl.util.error
.. automodule:: titan.core.sdl.util.controller
.. automodule:: titan.core.sdl.util.font
.. automodule:: titan.core.sdl.util.surface
