Utilities
=========

Types
-----

.. automodule:: titan.util.types

Decorators
----------

.. automodule:: titan.util.decorators

Patterns
--------

.. automodule:: titan.util.patterns
