import os
import sys
import platform
import subprocess
from setuptools import setup, Extension

# Some build options
USE_CYTHON = True
CYTHON_DEBUG = False

# Function for reading text files
def _read(file_name):
    return open(os.path.join(os.path.dirname(__file__), file_name)).read()

# Ship with raw C files
ext_type = '.pyx' if USE_CYTHON else '.c'

# OS X specific settings
if platform.system() == 'Darwin':

    # MacPorts
    if sys.prefix.startswith('/opt/local'):

        sdl_cflags = str(subprocess.check_output(['/opt/local/bin/sdl2-config', '--cflags']), 'utf-8')
        sdl_libs = str(subprocess.check_output(['/opl/local/bin/sdl2-config', '--libs']), 'utf-8')

        ext_args = dict(include_dirs=[],
                        library_dirs=[],
                        libraries=['SDL2', 'SDL2_image', 'SDL2_mixer', 'SDL2_ttf'],
                        extra_compile_args=['-Wno-unused-function'] + sdl_cflags.split(),
                        extra_link_args=[] + sdl_libs.split())

    # Vanilla
    else:

        ext_args = dict(include_dirs=['/Library/Frameworks/SDL2.framework/Headers',
                                      '/Library/Frameworks/SDL2_image.framework/Headers',
                                      '/Library/Frameworks/SDL2_mixer.framework/Headers',
                                      '/Library/Frameworks/SDL2_ttf.framework/Headers'],
                        library_dirs=[],
                        libraries=[],
                        extra_compile_args=['-Wno-unused-function'],
                        extra_link_args=['-framework', 'SDL2',
                                         '-framework', 'SDL2_image',
                                         '-framework', 'SDL2_mixer',
                                         '-framework', 'SDL2_ttf'])

# Windows specific settings
elif platform.system() == 'Windows':

    ext_args = dict(include_dirs=['C:\SDL2-2.0.3\include',
                                  'C:\SDL2_image-2.0.0\include',
                                  'C:\SDL2_mixer-2.0.0\include',
                                  'C:\SDL2_ttf-2.0.12\include',
                                  'C:\Program Files (x86)\Windows Kits\8.0\Include\shared'],
                    library_dirs=['C:\SDL2-2.0.3\lib\\x86',
                                  'C:\SDL2_image-2.0.0\lib\\x86',
                                  'C:\SDL2_mixer-2.0.0\lib\\x86',
                                  'C:\SDL2_ttf-2.0.12\lib\\x86'],
                    libraries=['SDL2', 'SDL2_image', 'SDL2_mixer', 'SDL2_ttf'],
                    extra_compile_args=[],
                    extra_link_args=[])

# Fallback settings
else:

    sdl_cflags = str(subprocess.check_output(['sdl2-config', '--cflags']), 'utf-8')
    sdl_libs = str(subprocess.check_output(['sdl2-config', '--libs']), 'utf-8')

    ext_args = dict(include_dirs=[],
                    library_dirs=[],
                    libraries=['SDL2', 'SDL2_image', 'SDL2_mixer', 'SDL2_ttf'],
                    extra_compile_args=[] + sdl_cflags.split(),
                    extra_link_args=[] + sdl_libs.split())

# Cython/C extensions
ext = [

    # titan.core.sdl
    Extension('titan.core.sdl.error',
        ['titan/core/sdl/error' + ext_type], **ext_args),
    Extension('titan.core.sdl.sdl_backend',
        ['titan/core/sdl/sdl_backend' + ext_type], **ext_args),

    # titan.core.sdl.subsystems
    Extension('titan.core.sdl.subsystems.display',
        ['titan/core/sdl/subsystems/display' + ext_type], **ext_args),
    Extension('titan.core.sdl.subsystems.audio',
        ['titan/core/sdl/subsystems/audio' + ext_type], **ext_args),
    Extension('titan.core.sdl.subsystems.input',
        ['titan/core/sdl/subsystems/input' + ext_type], **ext_args),

    # titan.core.sdl.subsystems.util
    Extension('titan.core.sdl.subsystems.util.font',
        ['titan/core/sdl/subsystems/util/font' + ext_type], **ext_args),
    Extension('titan.core.sdl.subsystems.util.surface',
        ['titan/core/sdl/subsystems/util/surface' + ext_type], **ext_args),
    Extension('titan.core.sdl.subsystems.util.controller',
        ['titan/core/sdl/subsystems/util/controller' + ext_type], **ext_args)

]

# Use Cythonize
if USE_CYTHON:
    from Cython.Build import cythonize
    ext = cythonize(ext, gdb_debug=CYTHON_DEBUG)

# Run setup
setup(name='Titan Platform',
      version=_read('VERSION'),
      description='2D indie game development platform',
      long_description=_read('README.md'),
      author='Titan Games',
      author_email='dev@titangames.com',
      url='http://bitbucket.org/titangames/titan_platform',
      packages=['titan',
                'titan.core',
                'titan.core.sdl',
                'titan.tools',
                'titan.util'],
      py_modules=[],
      ext_modules=ext,
      include_package_data=True,
      install_requires=['sdl2_cython>=0.2.1',
                        'webcolors>=1.5'])
