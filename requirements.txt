# Required
Cython>=0.22
sdl2_cython>=0.2.1
webcolors>=1.5

# Optional
Sphinx>=1.3.1
pep8>=1.6.2
pylint>=1.4.3
