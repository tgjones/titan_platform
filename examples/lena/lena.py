from titan.core import Game, Backend
from titan.extra import draw_background
from titan.util import on_down

class Lena(Game):

    def __init__(self, config):
        super().__init__(config)

    def draw_lena(self, **kwargs):
        draw_background('lena.bmp')

    @on_down
    def hello_lena(self, **kwargs):
        Backend().sys_message('Hello Lena!')

    @on_down
    def oohlala(self, **kwargs):
        Backend().sys_message('Oohlala! (x=' + str(kwargs['x']) + ', y=' + str(kwargs['y']) + ')')
        Backend().audio.play_sound('moan.ogg')


if __name__ == "__main__":

    import os.path as pth
    json_file = pth.join(pth.dirname(__file__), 'lena.json')
    game = Lena.from_json(json_file)
    game.start_game_loop()
