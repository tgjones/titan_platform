from titan.util import Rect, on_down
from titan.extra import TmxMap, Camera, \
    CollidableSprite, AnimatedSprite, SpriteSheet
from titan.core import Game, Backend, EventDispatcher


class Hero(CollidableSprite, AnimatedSprite):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        EventDispatcher().register_callback('frame_begin', self._update)
        self._velocity = 0

    def __del__(self):
        EventDispatcher().deregister_callback('frame_begin', self._update)

    def _update(self, *args, **kwargs):

        gravity = 2
        c = kwargs['c'] if 'c' in kwargs else 0.2
        self._velocity = self._velocity + gravity * c
        self.move(0, self._velocity * c)


class Platformer(Game):

    def __init__(self, config):
        super().__init__(config)
        self._current_level = None

    def load_level(self, map_file, **kwargs):
        Backend().sys_message('Loading \'' + map_file + '\'...')
        self._current_level = TmxMap(map_file)
        self._hero = Hero.from_json('hero.json')
        self._hero.start_animation('run', loop=True)

    @on_down
    def move_camera(self, x, y, **kwargs):
        Camera().move(x, y)


if __name__ == "__main__":

    import os.path as pth
    json_file = pth.join(pth.dirname(__file__), 'platformer.json')
    game = Platformer.from_json(json_file)
    game.start_game_loop()
