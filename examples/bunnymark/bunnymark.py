import random
from titan.core import Game, Backend
from titan.util import on_down
from titan.extra import Sprite, draw_background

random.seed()


class Bunny(Sprite):

    def __init__(self):
        super().__init__('wabbit_alpha.png')
        self._speed_x = 0
        self._speed_y = 0

    @property
    def speed_x(self):
        return self._speed_x

    @speed_x.setter
    def speed_x(self, value):
        self._speed_x = value

    @property
    def speed_y(self):
        return self._speed_y

    @speed_y.setter
    def speed_y(self, value):
        self._speed_y = value


class Bunnymark(Game):

    def __init__(self, config):
        super().__init__(config)
        self._bunny_count = 0
        self._bunnies = []

    @property
    def bunny_count(self):
        return self._bunny_count

    def draw_grass(self, img, **kwargs):
        draw_background(img, tile=True)

    @on_down
    def spawn_bunnies(self, count, **kwargs):

        for i in range(count):

            bunny = Bunny()
            bunny.x = kwargs['x'] if 'x' in kwargs else 0
            bunny.y = kwargs['y'] if 'y' in kwargs else 0

            bunny.speed_x = 2 + random.random() * 8
            bunny.speed_y = 2 + random.random() * 8

            self._bunnies.append(bunny)

        self._bunny_count += count

        Backend().sys_message('Total bunnies: ' + str(self.bunny_count))

    def animate_bunnies(self, gravity, **kwargs):

        min_x, min_y = 0, 0
        max_x = Backend().display.viewport.width
        max_y = Backend().display.viewport.height
        max_x -= 26
        max_y -= 37

        for bunny in self._bunnies:

            # Traverse
            bunny.x += bunny.speed_x
            bunny.y += bunny.speed_y
            bunny.y += gravity

            # Right side
            if bunny.x > max_x:
                bunny.speed_x *= -1
                bunny.x = max_x

            # Left side
            if bunny.x < min_x:
                bunny.speed_x *= -1
                bunny.x = min_x

            # Bottom
            if bunny.y > max_y:
                bunny.speed_y *= -0.8
                bunny.y = max_y
                if random.random() > 0.5:
                    bunny.speed_y -= random.randint(0, 10)

            # Top
            if bunny.y < min_y:
                bunny.speed_y *= -1
                bunny.y = min_y


if __name__ == "__main__":

    import os.path as pth
    json_file = pth.join(pth.dirname(__file__), 'bunnymark.json')
    game = Bunnymark.from_json(json_file)
    game.start_game_loop()
